<%--
 * list.jsp
 *
 * Copyright (C) 2015 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<form:form method="get" role="search" action="newspaper/actor/search.do?keyword=${text}">

		<input type="text" name="keyword" />
		<input type="submit" name="find"  value="
		<spring:message code="newspaper.search"/>" />

</form:form>

<div>
	<jstl:choose>
		<jstl:when test="${newspapers.size() != 0}">

			<div class="container">
				<table class="table table-bordered">
					<c:forEach items="${newspapers}" var="x">
						<tr>
							<td >
								<jstl:choose>
									<jstl:when test="${x.picture != null}">
										<img src="${x.picture.image}" style="width: 30%; height:auto;">
									</jstl:when>
									<jstl:otherwise>
										<p><b><spring:message code="image.empty"></spring:message></b></p>
									</jstl:otherwise>

								</jstl:choose>
							</td>
							<td >
								<div><p><a href="newspaper/actor/view.do?newspaperId=${x.id}">${x.title}</a> </p>
								</div>
							</td>
							<jstl:if test="${mine && x.draft == true}">
							<td >
								<div><p><acme:cancel code="standard.edit" url="newspaper/user/edit.do?newspaperId=${x.id}"/></p>
								</div>
							</td>
							</jstl:if>
						</tr>
					</c:forEach>
				</table>
			</div>
		</jstl:when>
		<jstl:otherwise>
			<p><b><spring:message code="newspaper.empty"></spring:message></b></p>
		</jstl:otherwise>

	</jstl:choose>


</div>