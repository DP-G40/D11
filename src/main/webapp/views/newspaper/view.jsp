<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
 * create.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>



<security:authentication property="principal" var="principal"/>

<jstl:choose>
    <jstl:when test="${newspaper.picture != null}">
        <div align="center">
            <img src="${newspaper.picture.image}" style="width: 20%;  height:auto;">
        </div>
    </jstl:when>
    <jstl:otherwise>

    </jstl:otherwise>

</jstl:choose>
<div>
    <table class="table table-bordered">
        <thead>
        <th colspan="2"><h1 align="center">${newspaper.title}</h1></th>
        </thead>


        <tr>
            <th><spring:message code="newspapers.publicationDate"></spring:message></th>
            <td>${newspaper.publicationDate}</td>
        </tr>

        <tr>
            <th><spring:message code="newspapers.description"></spring:message></th>
            <td>${newspaper.description}</td>
        </tr>


    </table>
</div>


<jstl:choose>
    <jstl:when test="${newspaper.privateFlag == true}">
        <security:authorize access="isAnonymous()">
            <div class="container">
                <div class="well">
                    <h2><spring:message code="only.customer.message"></spring:message></h2>
                </div>
            </div>
        </security:authorize>

        <security:authorize access="hasRole('USER')">
            <jstl:choose>
                <jstl:when test="${newspaper.user.id == actorId  }">
                    <jstl:choose>
                        <jstl:when test="${articles.size() != 0}">
                            <div class="container">
                                <c:forEach items="${articles}" var="x">
                                    <div class="row ">
                                        <div><p><b><spring:message code="articles.title"></spring:message></b>:<a
                                                href="article/actor/view.do?articleId=${x.id}"> ${x.title}</a></p>
                                        </div>
                                        <div><p><b><spring:message code="articles.author"></spring:message></b>:<a
                                                href="user/profiles/view.do?UserId=${x.user.id}"> ${x.user.name}</a></p>
                                        </div>

                                        <div><p><b><spring:message
                                                code="articles.publicationDate"></spring:message></b>: ${x.publicationDate}</p>
                                        </div>
                                        <div><p><b><spring:message code="articles.summary"></spring:message></b>: ${x.summary}
                                        </p>
                                        </div>
                                    </div>
                                </c:forEach>
                            </div>
                        </jstl:when>
                        <jstl:otherwise>
                            <p><spring:message code="articles.empty"></spring:message></p>
                        </jstl:otherwise>
                    </jstl:choose>
                </jstl:when>
                <jstl:otherwise>
                    <div class="container">
                        <div class="well">
                            <h2><spring:message code="only.customer.message"></spring:message></h2>
                        </div>
                    </div>
                </jstl:otherwise>
            </jstl:choose>

        </security:authorize>





        <security:authorize access="hasRole('CUSTOMER')">
            <jstl:choose>
                <jstl:when test="${articles.size() != 0 && mostrarBotonSub==false}">
                    <div class="container">
                        <c:forEach items="${articles}" var="x">
                            <div class="row ">
                                <div><p><b><spring:message code="articles.title"></spring:message></b>:<a
                                        href="article/actor/view.do?articleId=${x.id}"> ${x.title}</a></p>
                                </div>
                                <div><p><b><spring:message code="articles.author"></spring:message></b>:<a
                                        href="user/profiles/view.do?UserId=${x.user.id}"> ${x.user.name}</a></p>
                                </div>

                                <div><p><b><spring:message
                                        code="articles.publicationDate"></spring:message></b>: ${x.publicationDate}</p>
                                </div>
                                <div><p><b><spring:message code="articles.summary"></spring:message></b>: ${x.summary}
                                </p>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </jstl:when>
                <jstl:when test="${articles.size() != 0 && mostrarBotonSub==true}">
                    <div class="container">
                        <p><spring:message code="customer.must.sub"></spring:message></p>
                    </div>
                </jstl:when>
                <jstl:otherwise>
                    <p><spring:message code="articles.empty"></spring:message></p>
                </jstl:otherwise>
            </jstl:choose>
        </security:authorize>

        <security:authorize access="hasRole('ADMIN')">
            <jstl:choose>
                <jstl:when test="${articles.size() != 0}">
                    <div class="container">
                        <c:forEach items="${articles}" var="x">
                            <div class="row ">
                                <div><p><b><spring:message code="articles.title"></spring:message></b>:<a
                                        href="article/actor/view.do?articleId=${x.id}"> ${x.title}</a></p>
                                </div>
                                <div><p><b><spring:message code="articles.author"></spring:message></b>:<a
                                        href="user/profiles/view.do?UserId=${x.user.id}"> ${x.user.name}</a></p>
                                </div>

                                <div><p><b><spring:message
                                        code="articles.publicationDate"></spring:message></b>: ${x.publicationDate}</p>
                                </div>
                                <div><p><b><spring:message code="articles.summary"></spring:message></b>: ${x.summary}
                                </p>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </jstl:when>
                <jstl:otherwise>
                    <p><spring:message code="articles.empty"></spring:message></p>
                </jstl:otherwise>
            </jstl:choose>
        </security:authorize>

        <security:authorize access="hasRole('CUSTOMER')">

            <jstl:if test="${mostrarBotonSub==true}">
                <div class="container">
                    <h2><spring:message code="customer.sub"></spring:message></h2>
                    <div class="well">

                        <acme:cancel code="newspaper.customer.subscribirse"
                                     url="/subscription/customer/create.do?newspaperId=${newspaper.id}"/>
                    </div>
                </div>
            </jstl:if>

            <jstl:if test="${mostrarBotonSub==false}">
                <div class="container">
                    <h2><spring:message code="customer.sub"></spring:message></h2>
                    <div class="well">
                        <h2><spring:message code="customer.already.sub"></spring:message></h2>
                    </div>
                </div>
            </jstl:if>

        </security:authorize>
    </jstl:when>








    <jstl:otherwise>
        <jstl:choose>
            <jstl:when test="${articles.size() != 0}">
                <div class="container">
                    <c:forEach items="${articles}" var="x">
                        <div class="row ">
                            <div><p><b><spring:message code="articles.title"></spring:message></b>:<a
                                    href="article/actor/view.do?articleId=${x.id}"> ${x.title}</a></p>
                            </div>
                            <div><p><b><spring:message code="articles.author"></spring:message></b>:<a
                                    href="user/profiles/view.do?UserId=${x.user.id}"> ${x.user.name}</a></p>
                            </div>

                            <div><p><b><spring:message
                                    code="articles.publicationDate"></spring:message></b>: ${x.publicationDate}</p>
                            </div>
                            <div><p><b><spring:message code="articles.summary"></spring:message></b>: ${x.summary}
                            </p>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </jstl:when>
            <jstl:otherwise>
                <p><spring:message code="articles.empty"></spring:message></p>
            </jstl:otherwise>
        </jstl:choose>
    </jstl:otherwise>

</jstl:choose>



<security:authorize access="hasRole('ADMIN')">

    <div class="container">
        <h2><spring:message code="admin.panel"></spring:message></h2>
        <div class="well">
            <p><spring:message code="newspaper.remove.info.panel"></spring:message></p>
            <acme:cancel code="standard.remove"
                         url="/newspaper/administrator/remove.do?newspaperId=${newspaper.id}"/>
        </div>
    </div>


</security:authorize>
