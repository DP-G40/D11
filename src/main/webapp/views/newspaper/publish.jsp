
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<br><br>
<form:form action="newspaper/user/publish.do" modelAttribute="publishForm" method="post">

    <acme:select path="newspaper" code="newspaper.newspapers" items="${newspapers}" itemLabel="title" disabled="${disable}"/>

    <acme:checkbox path="privateFlag" code="newspaper.private"/>

    <br><br>
    <div class="col-xs-12 form-group">
        <acme:submit name="save" code="user.save"/>
        <acme:cancel code="user.cancel" url="/#" />
    </div>
    </div>
</form:form>
