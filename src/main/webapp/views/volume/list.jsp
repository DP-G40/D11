<%@page import="com.lowagie.tools.plugins.AbstractTool.Console"%>
<%@ page import="java.util.Date" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%--
 * create.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<security:authentication property="principal" var="principal"/>


<display:table name="volumes" id="volume" requestURI="/volume/view.do">
    <display:column property="title" titleKey="volume.title"/>
    <display:column property="description" titleKey="volume.description"/>
    <display:column property="year" titleKey="volume.year"/>
    <jstl:if test="${mine}">
    <security:authorize access="hasRole('USER')">
        <display:column titleKey="standard.edit">
            <jstl:if test="${volume.user.userAccount.id == principal.id}">
                <acme:cancel code="standard.edit" url="/volume/user/edit.do?volumeId=${volume.id}"/>
            </jstl:if>
        </display:column>
    </security:authorize>
    </jstl:if>
    <display:column titleKey="standard.view">
        <acme:cancel code="standard.view" url="/volume/view.do?volumeId=${volume.id}"/>
    </display:column>
    <security:authorize access="hasRole('CUSTOMER')">
        <display:column titleKey="customer.sub">
                <acme:cancel code="customer.sub" url="/subscription/customer/subscribeVolume.do?volumeId=${volume.id}"/>
        </display:column>
    </security:authorize>
</display:table>