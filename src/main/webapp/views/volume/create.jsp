
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<br><br>
<form:form action="${action}" modelAttribute="volumeForm" method="post">
    <form:hidden path="volumeId"/>

    <acme:textbox path="title" code="volume.title"/>
    <acme:textbox path="description" code="volume.description"/>
    <acme:textbox path="year" code="volume.year" type="number" min="1900" max="9999" step="1" value="2018"/>




    <br><br>
    <div class="col-xs-12 form-group">
        <acme:submit name="save" code="user.save"/>
        <jstl:if test="${volumeForm.volumeId != 0}">
            <acme:delete name="delete" code="standard.remove"/>
        </jstl:if>
        <acme:cancel code="user.cancel" url="/#" />
    </div>
    </div>
</form:form>
