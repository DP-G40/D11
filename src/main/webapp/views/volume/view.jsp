<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %><%--
<%--
 * list.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<security:authentication property="principal" var="principal"/>

<display:table name="volume" id="row" requestURI="/volume/view.do">
    <display:column property="title" titleKey="volume.title"/>
    <display:column property="description" titleKey="volume.description"/>
    <display:column property="year" titleKey="volume.year"/>
</display:table>

<display:table name="volume.newspapers" id="newspaper" requestURI="/volume/view.do">
    <display:column property="title" titleKey="volume.title"/>
    <display:column property="description" titleKey="volume.description"/>
    <display:column titleKey="standard.view">
        <acme:cancel code="standard.view" url="/newspaper/actor/view.do?newspaperId=${newspaper.id}"/>
    </display:column>
    <security:authorize access="hasRole('USER')">
            <display:column titleKey="volume.quit">
                <jstl:if test="${volume.user.userAccount.id == principal.id}">
                    <acme:cancel code="volume.quit" url="/volume/user/quit.do?volumeId=${volume.id}&newspaperId=${newspaper.id}"/>
                </jstl:if>
            </display:column>
    </security:authorize>
</display:table>
<br><br>
<security:authorize access="hasRole('USER')">
    <jstl:if test="${volume.user.userAccount.id == principal.id}">
        <acme:cancel code="volume.add" url="/volume/user/add.do?volumeId=${volume.id}"/>
    </jstl:if>
</security:authorize>