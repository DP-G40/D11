<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--
 * create.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="jslt" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="string" uri="http://java.sun.com/jsp/jstl/fmt" %>

<security:authentication property="principal" var="principal"/>


<div class="col-md-12">
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <table class="table table-bordered">
            <thead>
            <th colspan="2"><h1 align="center">${user.name} ${user.surname}</h1></th>
            </thead>
            <tr>
                <th><spring:message code="user.email"></spring:message></th>
                <td>${user.email}</td>
            </tr>
            <tr>
                <th><spring:message code="user.phoneNumber"></spring:message></th>
                <td>${user.phoneNumber}</td>
            </tr>
            <tr>
                <th><spring:message code="user.direction"></spring:message></th>
                <td>${user.direction}</td>
            </tr>


        </table>
    </div>
    <div class="col-md-4"></div>
</div>
</br>

<jstl:if test="${follow != null}">
<div class="container">
    <h2><spring:message code="user.follow.panel"></spring:message></h2>
    <div class="well">
        <jstl:choose>
            <jstl:when test="${follow == false}">
                    <acme:follow code="user.follow" url="/user/profiles/follow.do?userId=${user.id}"></acme:follow>

            </jstl:when>
            <jstl:otherwise>
                    <acme:unfollow code="user.unfollow" url="/user/profiles/unfollow.do?userId=${user.id}"></acme:unfollow>
            </jstl:otherwise>

    </jstl:choose>

    </div>
</div>
</jstl:if>

</br>
<div>
    <div>
        <div class="col-md-1"></div>
        <div class="col-md-11">
            <h2><spring:message code="user.list.articles"/></h2>
        </div>

    </div>

    <jstl:choose>
        <jstl:when test="${articles.size() != 0}">
            <div class="container">
                <table class="table table-bordered">
                    <tr>
                        <th><spring:message code="articles.title"></spring:message></th>
                        <th><spring:message
                                code="articles.publicationDate"></spring:message></th>
                        <th><spring:message code="articles.summary"></spring:message></th>

                    </tr>

                    <jslt:forEach items="${articles}" var="articulo">

                        <tr>
                            <td><a href="article/actor/view.do?articleId=${articulo.id}"> ${articulo.title}</a></td>
                            <td>${articulo.publicationDate}</td>
                            <td>${articulo.summary}</td>
                        </tr>


                    </jslt:forEach>
                </table>
            </div>
        </jstl:when>
        <jstl:otherwise>
            <p><spring:message code="articles.empty"></spring:message></p>
        </jstl:otherwise>

    </jstl:choose>


</div>

<security:authorize access="isAuthenticated()">
    <div>
        <div class="col-md-1"></div>
        <div class="col-md-11">
            <h2><spring:message code="chirp.mylist"/></h2>
        </div>

    </div>

    <display:table name="chirps" id="row" requestURI="${requestURI}">
        <display:column property="title" titleKey="articles.title"/>
        <display:column property="publicationDate" titleKey="articles.publicationDate"/>
        <display:column property="description" titleKey="newspapers.description"/>
    </display:table>
</security:authorize>


