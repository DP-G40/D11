<%@page import="com.lowagie.tools.plugins.AbstractTool.Console"%>
<%@ page import="java.util.Date" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%--
 * create.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<security:authentication property="principal" var="principal"/>

<c:choose>
    <c:when test="${chirps.size() != 0}">
        <display:table name="chirps" id="row" requestURI="${requestURI}">
            <display:column property="title" titleKey="articles.title"/>
            <display:column property="publicationDate" titleKey="articles.publicationDate"/>
            <display:column property="description" titleKey="newspapers.description"/>
            <security:authorize access="hasRole('ADMIN')">
                <display:column titleKey="standard.remove">
                    <acme:cancel url="/chirp/administrator/remove.do?chirpId=${row.id}"  code="standard.remove"></acme:cancel>

                </display:column>

            </security:authorize>
        </display:table>
    </c:when>
    <c:otherwise>
        <spring:message code="stream.empty"></spring:message>
    </c:otherwise>

</c:choose>

