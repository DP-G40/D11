<%@page import="com.lowagie.tools.plugins.AbstractTool.Console"%>
<%@ page import="java.util.Date" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%--
 * create.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<security:authentication property="principal" var="principal"/>

<form:form action="${action}" modelAttribute="chirpForm" method="post">
    <form:hidden path="chirpId"></form:hidden>
    <acme:textbox path="title" code="articles.title"></acme:textbox>
    <acme:textarea path="description" code="newspapers.description"></acme:textarea>



    <br><br>
    <div class="col-xs-12 form-group">
        <acme:submit name="save" code="user.save"/>
        <jstl:if test="${chirpForm.chirpId != 0}">
            <acme:delete name="delete" code="standard.remove"/>
        </jstl:if>
        <acme:cancel code="user.cancel" url="/#" />
    </div>
    </div>


</form:form>
