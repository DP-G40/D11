<%@page language="java" contentType="text/html; charset=ISO-8859-1"
		pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>


<form:form action="${action}" modelAttribute="actorForm" method="POST">

	<acme:textbox code="user.name" path="name"/>
	<acme:textbox code="user.surname" path="surname"/>
	<acme:textbox code="user.email" path="email"/>
	<acme:textbox code="user.phone" path="phoneNumber"/>
	<acme:textbox code="user.direction" path="direction"/>

<tr>
			<td colspan="3">
				<button type="submit" name="save" class="btn btn-primary"
						onclick="return validateFeedback()">
					<spring:message code="user.save" />
				</button>&nbsp;
				<acme:cancel code="actor.cancel" url="/#" />
			</td>
		</tr>

</form:form>

<script type="text/javascript">
    function validateFeedback() {
        var phone = document.getElementById("phoneNumber");
        var RE = /(\+\d{1,3} \d{1,3} \d{4,}$) |(\+\d{1,3} \d{4,}$) |(\d{4,}$)/;
        if (!(phone.value).match(RE)) {
            alert('<spring:message code="user.edit.data.phone.error" />');
        }
        return confirm('<spring:message code="row.confirm.edit" />');
    }
</script>