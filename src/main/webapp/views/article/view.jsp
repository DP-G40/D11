<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %><%--
<%--
 * list.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 *
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<security:authentication property="principal" var="principal"/>

<%
    Date date = new Date();
    SimpleDateFormat sp = new SimpleDateFormat("yyyy-MM-dd");
    String now = sp.format(date);
    pageContext.setAttribute("now", now);
%>
<div>
    <img class="img-responsive center-block" src="${banner }"
            style="margin-bottom: -10px; margin-top: -25px;" />
</div>

<div>
    <table class="table table-bordered">
        <thead>
        <th colspan="2"><h1 align="center">${article.title}</h1></th>
        </thead>
        <tr>
            <th><spring:message code="articles.author"></spring:message></th>
            <td><a href="user/profiles/view.do?UserId=${article.user.id}"> ${article.user.name}</a></td>
        </tr>

        <tr>
            <th><spring:message code="articles.publicationDate"></spring:message></th>
            <td>${article.publicationDate}</td>
        </tr>
        <tr>
            <th><spring:message code="articles.summary"></spring:message></th>
            <td>${article.summary}</td>
        </tr>
        <tr>
            <th><spring:message code="articles.body"></spring:message></th>
            <td>${article.body}</td>
        </tr>

    </table>
</div>


<jstl:choose>
    <jstl:when test="${article.pictures.size() != 0}">
        <c:forEach items="${article.pictures}" var="pic">
            <img src="${pic.image}" style="width: 30%; height:auto;">
            <p></p>
        </c:forEach>
    </jstl:when>
    <jstl:otherwise>
        <p><b><spring:message code="image.empty.article"></spring:message></b></p>
    </jstl:otherwise>

</jstl:choose>

<div>
    <h2><spring:message code="followup.pral"></spring:message></h2>
    <table class="table-striped">
        <c:forEach items="${article.followUps}" var="fup">
            <thead>
            <th colspan="2">
                <center>${fup.title}</center>
            </th>
            </thead>
            <tbody>
            <tr>
                <th><spring:message code="articles.publicationDate"></spring:message></th>
                <td>${fup.publicationDate}</td>
            </tr>
            <tr>
                <th><spring:message code="fup.symmary"></spring:message></th>
                <td>${fup.summary}</td>

            </tr>
            <tr>
                <th><spring:message code="fup.text"></spring:message></th>
                <td>${fup.text}</td>

            </tr>
            <tr>

                <td>
                <jstl:if test="${actorId==article.user.id && article.draft==false && article.newspaper.draft==false}">
                    <acme:cancel code="article.addPicture"
                                     url="/followUp/user/picture.do?followUpId=${fup.id}"/>
                </jstl:if>
                </td>
                <td></td>
            </tr>
            <tr>
                <td>
                <jstl:choose>
                    <jstl:when test="${fup.pictures.size() != 0}">
                        <c:forEach items="${fup.pictures}" var="x">
                            <img src="${x.image}" style="width: 15%; height:auto;">

                        </c:forEach>
                    </jstl:when>
                    <jstl:otherwise>
                        <p><b><spring:message code="image.empty.followup"></spring:message></b></p>
                    </jstl:otherwise>

                </jstl:choose>
                </td>
                <td></td>
            </tr>


            </tbody>

        </c:forEach>
    </table>
</div>

<security:authorize access="hasRole('ADMIN')">

    <div class="container">
        <h2><spring:message code="admin.panel"></spring:message></h2>
        <div class="well">
            <p><spring:message code="article.remove.info.panel"></spring:message></p>
            <acme:cancel code="standard.remove"
                         url="/article/administrator/remove.do?articleId=${article.id}"/>
        </div>
    </div>


</security:authorize>

<security:authorize access="hasRole('USER')">
    <jstl:if test="${actorId==article.user.id && article.draft==false && article.newspaper.draft==false}">
        <div class="container">
            <h2><spring:message code="create.followUp"></spring:message></h2>
            <div class="well">
                <acme:cancel code="create.followUp"
                             url="/followUp/user/create.do?articleId=${article.id}"/>
            </div>
        </div>
    </jstl:if>

</security:authorize>