
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<br><br>
<form:form action="${action}" modelAttribute="articleForm" method="post">
    <form:hidden path="articleId"/>

    <acme:textbox path="title" code="articles.title"/>
    <acme:textbox path="summary" code="articles.summary"/>
    <acme:textarea path="body" code="articles.body"/>

    <acme:select path="newspaper" code="newspaper.newspapers" items="${newspaper}" itemLabel="title"/>



    <br><br>
    <div class="col-xs-12 form-group">
        <acme:submit name="save" code="user.save"/>
        <jstl:if test="${articleForm.articleId != 0}">
            <acme:success name="publish" code="article.publish"/>
            <acme:delete name="delete" code="standard.remove"/>
        </jstl:if>
        <acme:cancel code="user.cancel" url="/#" />
    </div>
    </div>
</form:form>
