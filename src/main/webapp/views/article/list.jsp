<%--
 * list.jsp
 *
 * Copyright (C) 2015 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security"
          uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>


<form:form method="get" role="search" action="article/actor/search.do?keyword=${text}">

    <input type="text" name="keyword"/>
    <input type="submit" name="find" value="
		<spring:message code="article.search"/>"/>

</form:form>


<display:table name="articles" id="article" requestURI="${requestURI}">

    <display:column titleKey="articles.title">
        <a href="article/actor/view.do?articleId=${article.id}">${article.title}</a>

    </display:column>
    <display:column titleKey="articles.publicationDate">
        <jstl:choose>
            <jstl:when test="${article.publicationDate == null}">
                <p><spring:message code="article.draft"></spring:message></p>
            </jstl:when>
            <jstl:otherwise>
                <p>${article.publicationDate}</p>
            </jstl:otherwise>

        </jstl:choose>


    </display:column >
    <display:column titleKey="articles.summary" property="summary"/>
    <display:column titleKey="articles.author">
        <a href="user/profiles/view.do?UserId=${article.user.id}">${article.user.name}</a>
    </display:column>

    <security:authorize access="hasRole('USER')">
        <security:authentication property="principal" var="principal"/>
        <jstl:if test="${mine == true && article.user.userAccount.id == principal.id }">
            <display:column titleKey="standard.edit" >
                <jstl:if test="${article.draft}">
                    <acme:cancel code="standard.edit" url="/article/user/edit.do?articleId=${article.id}"/>
                </jstl:if>
            </display:column>
            <display:column titleKey="article.addPicture" >
                <jstl:if test="${article.draft}">
                    <acme:cancel code="article.addPicture" url="/article/user/picture.do?articleId=${article.id}"/>
                </jstl:if>
            </display:column>
        </jstl:if>
    </security:authorize>

</display:table>


