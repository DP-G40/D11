<%--
 * action-2.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl"	uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<h2><spring:message	code="dashboard.Q1" />:</h2> <c:out value="${Q1[0][0]}"/> - <c:out value="${Q1[0][1]}"/><br>
<h2><spring:message	code="dashboard.Q2" />:</h2> <c:out value="${Q2[0][0]}"/> - <c:out value="${Q2[0][1]}"/><br>
<h2><spring:message	code="dashboard.Q3" />:</h2> <c:out value="${Q3[0][0]}"/> - <c:out value="${Q3[0][1]}"/><br>
<h2><spring:message	code="dashboard.Q4" /></h2><br>
<display:table name="Q4" id="Q4" requestURI="/administrator/dashboard.do">

	<display:column titleKey="newspapers.title" property="title"/>
	<display:column titleKey="standard.view"><acme:cancel url="newspaper/actor/view.do?newspaperId=${Q4.id}" code="standard.view" /></display:column>

</display:table>
<h2><spring:message	code="dashboard.Q5" /></h2><br>
<display:table name="Q5" id="Q5" requestURI="/administrator/dashboard.do">

	<display:column titleKey="newspapers.title" property="title"/>
	<display:column titleKey="standard.view"><acme:cancel url="newspaper/actor/view.do?newspaperId=${Q5.id}" code="standard.view" /></display:column>

</display:table>
<h2><spring:message	code="dashboard.Q6" />:</h2> <c:out value="${Q6}"/><br>
<h2><spring:message	code="dashboard.Q7" />:</h2> <c:out value="${Q7}"/><br>
<h2><spring:message	code="dashboard.Q8" />:</h2> <c:out value="${Q8}"/><br>
<h2><spring:message	code="dashboard.Q9" />:</h2> <c:out value="${Q9}"/><br>
<h2><spring:message	code="dashboard.Q10" />:</h2> <c:out value="${Q10}"/><br>
<h2><spring:message	code="dashboard.Q4" />:</h2> <c:out value="${Q11[0][0]}"/> - <c:out value="${Q11[0][1]}"/><br>
<h2><spring:message	code="dashboard.Q10" />:</h2> <c:out value="${Q12}"/><br>
<h2><spring:message	code="dashboard.Q14" />:</h2> <c:out value="${Q13}"/><br>
<h2><spring:message	code="dashboard.Q14" />:</h2> <c:out value="${Q14}"/><br>
<h2><spring:message	code="dashboard.Q15" />:</h2> <c:out value="${Q15}"/><br>
<h2><spring:message	code="dashboard.Q16" />:</h2> <c:out value="${Q16}"/><br>
<h2><spring:message	code="dashboard.Q17" />:</h2> <c:out value="${Q17}"/><br>

<h2><spring:message	code="dashboard.Q18" />:</h2> <c:out value="${Q18}"/><br>
<h2><spring:message	code="dashboard.Q19" />:</h2> <c:out value="${Q19}"/><br>
<h2><spring:message	code="dashboard.Q20" />:</h2> <c:out value="${Q20}"/><br>
<h2><spring:message	code="dashboard.Q21" />:</h2> <c:out value="${Q21}"/><br>
