
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<br><br>
<form:form action="tabooWord/administrator/save.do" modelAttribute="tabooWord" method="post">
	<form:hidden path="id"/>
	<form:hidden path="version"/>
	
    <acme:textbox path="word" code="tabooWord.word"/>

        <br><br>
        <div class="col-xs-12 form-group">
            <acme:submit name="save" code="user.save"/>
            <acme:cancel code="user.cancel" url="/#" />
        </div>
    </div>
</form:form>
