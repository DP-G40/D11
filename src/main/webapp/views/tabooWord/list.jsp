<%--
 * list.jsp
 *
 * Copyright (C) 2015 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>
<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<display:table name="tabooWords" id="tabooWord" requestURI="${requestURI}">

    <display:column titleKey="tabooWord.word" property="word"/>

	<display:column titleKey="standard.edit">
        <a href="tabooWord/administrator/edit.do?tabooWordId=${tabooWord.id}"><spring:message code="standard.edit"/></a>
    </display:column>
    
    <display:column titleKey="tabooWord.delete">
        <a href="tabooWord/administrator/delete.do?tabooWordId=${tabooWord.id}"><spring:message code="tabooWord.delete"/></a>
    </display:column>

</display:table>

<a href="tabooWord/administrator/create.do"><spring:message code="tabooWord.create"/></a><br>

<h2><spring:message	code="tabooWord.list.articles" /></h2><br>
<display:table name="tabooWordsArticles" id="tabooWordsArticle" requestURI="/tabooWord/administrator/list.do">

    <display:column titleKey="tabooWordsArticle.publicationDate" property="publicationDate"/>
    <display:column titleKey="tabooWordsArticle.summary" property="summary"/>
    <display:column titleKey="tabooWordsArticle.author" property="user.name"/>
    <display:column>
    	<acme:cancel code="standard.remove"
                                       url="/article/administrator/remove.do?articleId=${tabooWordsArticle.id}"/>
    </display:column>

</display:table>

<h2><spring:message	code="tabooWord.list.newspaper" /></h2><br>
<display:table name="tabooWordsNewspapers" id="tabooWordsNewspaper" requestURI="/tabooWord/administrator/list.do">

    <display:column titleKey="tabooWordsNewspaper.publicationDate" property="publicationDate"/>
    <display:column titleKey="tabooWordsNewspaper.title" property="title"/>
    <display:column titleKey="tabooWordsNewspaper.author" property="user.name"/>
    
    <display:column>
    	<acme:cancel code="standard.remove"
                                       url="/newspaper/administrator/remove.do?newspaperId=${tabooWordsNewspaper.id}"/>
    </display:column>

</display:table>

<h2><spring:message	code="tabooWord.list.chirp" /></h2><br>
<display:table name="tabooWordsChirps" id="tabooWordsChirp" requestURI="/tabooWord/administrator/list.do">

    <display:column titleKey="tabooWordsChirp.publicationDate" property="publicationDate"/>
    <display:column titleKey="tabooWordsChirp.title" property="title"/>
    <display:column titleKey="tabooWordsChirp.author" property="user.name"/>
    
    <display:column>
    	<acme:cancel code="standard.remove"
                                       url="/chirp/administrator/remove.do?chirpId=${tabooWordsChirp.id}"/>
    </display:column>

</display:table>