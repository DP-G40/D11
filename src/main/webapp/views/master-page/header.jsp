<%--
 * header.jsp
 *
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<meta charset="UTF-8">

<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<div>
	<a href=""><img src="images/logo.png" alt="Acme-Newspaper Co., Inc." /></a>
</div>
<link rel="stylesheet"
	  href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

<!-- jQuery library -->
<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<security:authentication property="principal" var="principal"/>
<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="">ACME NEWSPAPER</a>
		</div>
		<ul class="nav navbar-nav navbar-left">


		<security:authorize access="!isAuthenticated()">
			<li><a href="newspaper/actor/list.do"><spring:message code="master.page.newspapers" /></a></li>
			<li><a href="article/actor/listAll.do"><spring:message code="master.page.articles" /></a></li>
			<li><a href="user/profiles/list.do"><spring:message code="master.page.listUser"/></a></li>
			<li><a href="volume/list.do"><spring:message code="master.page.volume"/></a></li>
		</security:authorize>

		<security:authorize access="hasRole('CUSTOMER')">
			<li><a href="newspaper/actor/list.do"><spring:message code="master.page.newspapers" /></a></li>
			<li><a href="article/actor/listAll.do"><spring:message code="master.page.articles" /></a></li>
			<li><a href="volume/list.do"><spring:message code="master.page.volume"/></a></li>
		</security:authorize>

		<security:authorize access="hasRole('USER')">
			<li><a href="newspaper/actor/list.do"><spring:message code="master.page.newspapers" /></a></li>
			<li><a href="article/actor/listAll.do"><spring:message code="master.page.articles" /></a></li>
			<li><a href="user/profiles/list.do"><spring:message code="master.page.listUser"/></a></li>
			<li><a href="volume/list.do"><spring:message code="master.page.volume"/></a></li>
			<li class="dropdown"><a href="" class="dropdown-toggle"	data-toggle="dropdown"> <spring:message	code="master.page.Chirps"/> <span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li><a href="chirp/user/stream.do"><spring:message code="master.page.chirp.stream"/></a></li>
					<li><a href="chirp/user/create.do"><spring:message code="master.page.chirp.create"/></a></li>
					<li><a href="user/profiles/followers.do"><spring:message code="master.page.followers"/></a></li>
					<li><a href="user/profiles/following.do"><spring:message code="master.page.following"/></a></li>
				</ul>
			</li>

			<li class="dropdown"><a href="" class="dropdown-toggle"	data-toggle="dropdown"> <spring:message	code="master.page.myNewspapers"/> <span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li><a href="newspaper/user/list.do"><spring:message code="master.page.listmyNewspapers" /></a></li>
					<li><a href="newspaper/user/create.do"><spring:message code="master.page.create.newspapers" /></a></li>
					<li><a href="newspaper/user/publish.do"><spring:message code="master.page.newspaper.publish" /></a></li>
				</ul>
			</li>
			<li class="dropdown"><a href="" class="dropdown-toggle"	data-toggle="dropdown"> <spring:message	code="master.page.myArticles"/> <span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li><a href="article/user/list.do"><spring:message code="master.page.listmyArticles" /></a></li>
					<li><a href="article/user/create.do"><spring:message code="master.page.publish.article" /></a></li>

				</ul>
			</li>
			<li class="dropdown"><a href="" class="dropdown-toggle"	data-toggle="dropdown"> <spring:message	code="master.page.myVolumes"/> <span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li><a href="volume/user/create.do"><spring:message code="master.page.volume.create" /></a></li>
					<li><a href="volume/user/list.do"><spring:message code="master.page.volume.list" /></a></li>

				</ul>
			</li>


		</security:authorize>

			<security:authorize access="hasRole('ADMIN')">
				<li><a href="administrator/dashboard.do"><spring:message code="master.page.admin.dashboard" /></a></li>
				<li><a href="advertisement/administrator/listTaboo.do"><spring:message code="master.page.admin.advertisements" /></a></li>
				<li><a href="advertisement/administrator/list.do"><spring:message code="master.page.admin.advertisements.all" /></a></li>
				<li class="dropdown"><a href="" class="dropdown-toggle"
										data-toggle="dropdown"> <spring:message
						code="master.page.admin"/> <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="newspaper/actor/list.do"><spring:message code="master.page.newspapers.admin" /></a></li>
						<li><a href="article/actor/listAll.do"><spring:message code="master.page.articles.admin" /></a></li>
						<li><a href="chirp/administrator/list.do"><spring:message code="master.page.chirp.admin" /></a></li>
						<li><a href="tabooWord/administrator/list.do"><spring:message code="master.page.tabooWord.admin" /></a></li>

					</ul>
				<li><a href="volume/list.do"><spring:message code="master.page.volume"/></a></li>
				</li>

			</security:authorize>

			<security:authorize access="hasRole('AGENT')">
				<li class="dropdown"><a href="" class="dropdown-toggle"
										data-toggle="dropdown"> <spring:message
						code="master.page.agent"/> <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="advertisement/agent/create.do"><spring:message code="master.page.advertisment.publish" /></a></li>
					</ul>
				<li><a href="newspaper/agent/listAdd.do"><spring:message code="master.page.newspaper.add" /></a></li>
				<li><a href="newspaper/agent/listNoAdd.do"><spring:message code="master.page.newspaper.noadd" /></a></li>
				<li><a href="volume/list.do"><spring:message code="master.page.volume"/></a></li>
				</li>

			</security:authorize>
		</ul>



		<ul class="nav navbar-nav navbar-right">






			<security:authorize access="isAuthenticated()">
				<li class="dropdown"><a href="" class="dropdown-toggle"
										data-toggle="dropdown"> <spring:message
						code="master.page.message"/>  <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<security:authorize access="hasRole('ADMIN')">
							<li><a href="message/administrator/broadcast.do"><spring:message code="master.page.administrator.broadcast" /></a></li>
						</security:authorize>

						<li><a href="message/authenticate/create.do"><spring:message code="master.page.newmessage" /></a></li>
						<li><a href="folder/authenticate/list.do"><spring:message code="master.page.folders" /></a></li>
					</ul>
				</li>
				<li class="dropdown"><a href="" class="dropdown-toggle"
										data-toggle="dropdown"> <spring:message
						code="master.page.profile"/> (<security:authentication
						property="principal.username"/>) <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="user/profiles/view.do?UserId=${principal.id}"><spring:message
								code="master.page.myprofile.user"/> </a></li>
						<li><a href="actor/edit.do"><spring:message
								code="master.page.edit.user"/> </a></li>
						<li><a href="j_spring_security_logout"><spring:message
								code="master.page.logout"/> </a></li>
					</ul>
				</li>
			</security:authorize>





			<security:authorize access="isAnonymous()">
				<li><a href="security/login.do"><spring:message
						code="master.page.login"/></a></li>

				<li><a href="user/register.do"><spring:message
						code="master.page.register.user"/></a></li>

				<li><a href="customer/register.do"><spring:message
						code="master.page.register.customer"/></a></li>

				<li><a href="agent/register.do"><spring:message
						code="master.page.register.agent"/></a></li>
			</security:authorize>
			<li><a href="?language=en"><img class="flag"
											src="https://lipis.github.io/flag-icon-css/flags/4x3/us.svg"
											style="width: 24px;" alt="United States of America Flag"></a></li>
			<li><a href="?language=es"><img class="flag"
											src="https://lipis.github.io/flag-icon-css/flags/4x3/es.svg"
											style="width: 24px;" alt="Spain Flag"></a></li>
		</ul>




</nav>
<br>