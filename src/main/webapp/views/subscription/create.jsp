<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %><%--
 * action-1.jsp
 *
 * Copyright (C) 2013 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<%
	Date date = new Date();
	SimpleDateFormat sp = new SimpleDateFormat("yyyy-MM-dd");
	String now = sp.format(date);
	pageContext.setAttribute("now",now);
%>
<br><br>
<form:form action="${action}" modelAttribute="subscriptionForm" method="post">
	<form:hidden path="newspaperId"/>
	<form:hidden path="volumeId"/>
<div class="row">
	<div class="col-xs-12 form-group">
		<acme:textbox code="row.holderName" path="holderName"/>
	</div><div class="col-xs-12 form-group">
		<acme:textbox code="row.brand" path="brand"/>
	</div><div class="col-xs-12 form-group">
		<acme:textbox code="row.number" path="number" />
	</div><div class="col-xs-12 form-group">
		<acme:textbox code="row.month" path="expiryMonth" type="number" min="1" max="12"/>
	</div><div class="col-xs-12 form-group">
		<acme:textbox code="row.year" path="expiryYear" type="number" min="2018" max="9999"/>
	</div><div class="col-xs-12 form-group">
		<acme:textbox code="row.cvv" path="cvv" type="number" min="100" max="999"/>
	</div>
	
	<br><br>
	<div class="col-xs-12 form-group">
		<acme:submit name="save" code="customer.save"/>
		<acme:cancel code="customer.cancel" url="" />
	</div>
</div>
</form:form>

	
    
    
 
    
    