<%@page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1" %>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="display" uri="http://displaytag.sf.net" %>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags" %>

<br><br>
<form:form action="advertisement/agent/create.do" modelAttribute="advertisementForm" method="post">


    <div class="row">
        <div class="col-xs-12 form-group">
            <acme:textbox path="title" code="articles.title"/>
        </div>
        <div class="col-xs-12 form-group">
            <acme:textbox path="urlBanner" code="advertisement.banner"/>
        </div>
        <div class="col-xs-12 form-group">
            <acme:textbox path="urlPage" code="advertisement.page"/>
        </div>
        <div class="col-xs-12 form-group">
            <acme:select path="newspaper" code="newspaper.newspapers" items="${newspapers}" itemLabel="title"/>
        </div>

        <div class="col-xs-12 form-group">
            <acme:textbox code="row.holderName" path="creditCard.holderName"/>
        </div>
        <div class="col-xs-12 form-group">
            <acme:textbox code="row.brand" path="creditCard.brand"/>
        </div>
        <div class="col-xs-12 form-group">
            <acme:textbox code="row.number" path="creditCard.number" />
        </div>
        <div class="col-xs-12 form-group">
            <acme:textbox code="row.month" path="creditCard.expiryMonth" type="number" min="1" max="12"/>
        </div>
        <div class="col-xs-12 form-group">
            <acme:textbox code="row.year" path="creditCard.expiryYear" type="number" min="2018" max="9999"/>
        </div>
        <div class="col-xs-12 form-group">
            <acme:textbox code="row.cvv" path="creditCard.cvv" type="number" min="100" max="999"/>
        </div>


        <br><br>
        <div class="col-xs-12 form-group">
            <acme:submit name="save" code="customer.save"/>
            <acme:cancel code="customer.cancel" url=""/>
        </div>
    </div>
</form:form>
