<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %><%--
 * action-1.jsp
 *
 * Copyright (C) 2013 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the 
 * TDG Licence, a copy of which you may download from 
 * http://www.tdg-seville.info/License.html
 --%>

<%@page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>

<%@ taglib prefix="acme" tagdir="/WEB-INF/tags"%>

<%
	Date date = new Date();
	SimpleDateFormat sp = new SimpleDateFormat("yyyy-MM-dd");
	String now = sp.format(date);
	pageContext.setAttribute("now",now);
%>
<br><br>
<form:form action="customer/register.do" modelAttribute="registerForm" method="post">
<div class="row">
	<div class="col-xs-12 form-group">
		<acme:textbox code="customer.name" path="name" />
	</div><div class="col-xs-12 form-group">
		<acme:textbox code="customer.surname" path="surname" />
	</div><div class="col-xs-12 form-group">
		<acme:textbox code="customer.email" path="email" />
	</div><div class="col-xs-12 form-group">
		<acme:textbox code="customer.phone" path="phone" />
	</div><div class="col-xs-12 form-group">
		<acme:textbox code="customer.address" path="address" />
	</div><div class="col-xs-12 form-group">
		<acme:textbox code="customer.userName" path="userName" />
	</div><div class="col-xs-12 form-group">
		<acme:password code="customer.password" path="password" />
	</div><div class="col-xs-12 form-group">
		<acme:password code="customer.repeatPassword" path="repeatPassword" />
	</div>
	
	<br><br>
	<div class="col-xs-12 form-group">
		<acme:checkbox code="customer.termsAndConditions" path="check" /><br><br>
		<acme:submit name="save" code="customer.save"/>
		<acme:cancel code="customer.cancel" url="" />
	</div>
</div>
</form:form>
