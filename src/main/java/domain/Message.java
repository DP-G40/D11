package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Date;

@Entity
@Access(AccessType.PROPERTY)
public class Message extends DomainEntity implements Cloneable{

    // Atributos ----
    private String subject;
    private String body;
    private Date moment;
    private String priority;
    private Boolean spam = false;

    // Constructor ----

    public Message() {
        super();
    }


    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy/MM/dd HH:mm")
    public Date getMoment() {
        return moment;
    }

    public void setMoment(Date moment) {
        this.moment = moment;
    }

    @NotNull
    @NotBlank
    @Pattern(regexp = "HIGH|NEUTRAL|LOW")
    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    @NotNull
    public Boolean getSpam() {
        return spam;
    }

    public void setSpam(Boolean spam) {
        this.spam = spam;
    }

    //Relaciones

    private Actor sender;
    private Actor recipient;
    private Folder folder;

    @NotNull
    @ManyToOne(optional = false)
    public Actor getSender() {
        return sender;
    }

    public void setSender(Actor sender) {
        this.sender = sender;
    }

    @NotNull
    @ManyToOne(optional = false)
    public Actor getRecipient() {
        return recipient;
    }

    public void setRecipient(Actor recipient) {
        this.recipient = recipient;
    }

    @NotNull
    @ManyToOne(optional = false)
    public Folder getFolder() {
        return folder;
    }

    public void setFolder(Folder folder) {
        this.folder = folder;
    }

    @Override
    public Message clone() {
        Message copia = null;
        try {
            copia = (Message) super.clone();
        } catch (final CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return copia;
    }
}
