package domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Access(AccessType.PROPERTY)
public class User extends Actor {

    // Atributos ----

    // Constructor ----

    public User() {
        super();
    }

//Relaciones

    private Collection<Newspaper> newspapers = new ArrayList<Newspaper>();
    private Collection<Article> articles = new ArrayList<Article>();
    private Collection<FollowUp> followUps = new ArrayList<FollowUp>();
    private Collection<Chirp> chirps = new ArrayList<Chirp>();
    //USER QUE ME SIGUEN
    private Collection<User> followers = new ArrayList<User>();
    //USER A LOS QUE SIGO
    private Collection<User> following = new ArrayList<User>();
    private Collection<Volume> volumes = new ArrayList<Volume>();

    @NotNull
    @OneToMany(mappedBy = "user")
    public Collection<Newspaper> getNewspapers() {
        return newspapers;
    }

    public void setNewspapers(Collection<Newspaper> newspapers) {
        this.newspapers = newspapers;
    }

    @NotNull
    @OneToMany(mappedBy = "user")
    public Collection<Article> getArticles() {
        return articles;
    }

    public void setArticles(Collection<Article> articles) {
        this.articles = articles;
    }

    @NotNull
    @OneToMany(mappedBy = "user")
    public Collection<FollowUp> getFollowUps() {
        return followUps;
    }

    public void setFollowUps(Collection<FollowUp> followUps) {
        this.followUps = followUps;
    }

    @NotNull
    @OneToMany(mappedBy = "user")
    public Collection<Chirp> getChirps() {
        return chirps;
    }

    public void setChirps(Collection<Chirp> chirps) {
        this.chirps = chirps;
    }

    @ManyToMany(mappedBy="following")
    public Collection<User> getFollowers() {
        return followers;
    }

    public void setFollowers(Collection<User> followers) {
        this.followers = followers;
    }

    @ManyToMany(cascade = CascadeType.ALL)
    public Collection<User> getFollowing() {
        return following;
    }

    public void setFollowing(Collection<User> following) {
        this.following = following;
    }

    @NotNull
    @OneToMany(mappedBy = "user")
    public Collection<Volume> getVolumes() {
        return volumes;
    }

    public void setVolumes(Collection<Volume> volumes) {
        this.volumes = volumes;
    }
}
