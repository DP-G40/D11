package domain;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
@Table(indexes = {@Index(columnList = "newspaper_id"),@Index(columnList = "customer_id")})
public class Subscription extends DomainEntity {

    // Atributos ----
    private CreditCard creditcard;

    // Constructor ----

    public Subscription() {
        super();
    }

    @NotNull
    @Valid
    public CreditCard getCreditcard() {
        return creditcard;
    }

    public void setCreditcard(CreditCard creditcard) {
        this.creditcard = creditcard;
    }

    //Relaciones
    private Customer customer;
    private Newspaper newspaper;
    private Volume volume;

    @NotNull
    @ManyToOne
    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    @ManyToOne
    public Newspaper getNewspaper() {
        return newspaper;
    }

    public void setNewspaper(Newspaper newspaper) {
        this.newspaper = newspaper;
    }

    @ManyToOne
    public Volume getVolume() {
        return volume;
    }

    public void setVolume(Volume volume) {
        this.volume = volume;
    }
}
