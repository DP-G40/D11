package domain;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
@Table(indexes = {@Index(columnList = "word")})
public class TabooWord extends DomainEntity {

    // Atributos ----
    private String word;

    // Constructor ----

    public TabooWord() {
        super();
    }

    @NotNull
    @NotBlank
    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
}
