package domain;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Access(AccessType.PROPERTY)
public class Volume extends DomainEntity {

    // Atributos ----
    private String title;
    private String description;
    private String year;

    // Constructor ----

    public Volume() {
        super();
    }

    @NotNull
    @NotBlank
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @NotNull
    @NotBlank
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @NotNull
    @NotBlank
    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    //Relaciones

    private Collection<Newspaper> newspapers = new ArrayList<Newspaper>();
    private Collection<Subscription> subscriptions = new ArrayList<Subscription>();
    private User user;

    @NotNull
    @ManyToMany
    public Collection<Newspaper> getNewspapers() {
        return newspapers;
    }

    public void setNewspapers(Collection<Newspaper> newspapers) {
        this.newspapers = newspapers;
    }

    @NotNull
    @OneToMany(mappedBy = "volume")
    public Collection<Subscription> getSubscriptions() {
        return subscriptions;
    }

    public void setSubscriptions(Collection<Subscription> subscriptions) {
        this.subscriptions = subscriptions;
    }

    @NotNull
    @ManyToOne(optional = false)
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
