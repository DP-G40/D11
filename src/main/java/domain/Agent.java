package domain;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Access(AccessType.PROPERTY)
public class Agent extends Actor {

    // Atributos ----

    // Constructor ----

    public Agent() {
        super();
    }

    //Relaciones
    private Collection<Advertisement> advertisements = new ArrayList<Advertisement>();

    @NotNull
    @OneToMany(mappedBy = "agent")
    public Collection<Advertisement> getAdvertisements() {
        return advertisements;
    }

    public void setAdvertisements(Collection<Advertisement> advertisements) {
        this.advertisements = advertisements;
    }
}
