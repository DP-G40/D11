package domain;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;

@Entity
@Access(AccessType.PROPERTY)
public class Customer extends Actor {
 
 // Atributos ---- 
 
 // Constructor ---- 
 
 public Customer(){
 super();
}

//Relaciones

 private Collection<Subscription> subscriptions = new ArrayList<Subscription>();

 @NotNull
 @OneToMany(mappedBy = "customer")
 public Collection<Subscription> getSubscriptions() {
  return subscriptions;
 }

 public void setSubscriptions(Collection<Subscription> subscriptions) {
  this.subscriptions = subscriptions;
 }
}
