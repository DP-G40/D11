package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Entity
@Access(AccessType.PROPERTY)
@Table(indexes = {@Index(columnList = "newspaper_id"),@Index(columnList = "draft")})
public class Article extends DomainEntity {

    // Atributos ----


    private String title;
    private Date publicationDate;
    private String summary;
    private String body;
    private Boolean draft = true;

    // Constructor ----

    public Article() {
        super();
    }

    @NotNull
    @NotBlank
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @DateTimeFormat(pattern = "yyyy/MM/dd HH:mm")
    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    @NotNull
    @NotBlank
    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    @NotNull
    @NotBlank
    @Column(length=512)
    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @NotNull
    public Boolean getDraft() {
        return draft;
    }

    public void setDraft(Boolean draft) {
        this.draft = draft;
    }

    private User user;
    private Newspaper newspaper;
    private Collection<Picture> pictures = new ArrayList<Picture>();
    private Collection<FollowUp> followUps = new ArrayList<FollowUp>();

    @NotNull
    @ManyToOne
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @NotNull
    @ManyToOne
    public Newspaper getNewspaper() {
        return newspaper;
    }

    public void setNewspaper(Newspaper newspaper) {
        this.newspaper = newspaper;
    }

    @NotNull
    @OneToMany(mappedBy = "article",cascade = CascadeType.REMOVE)
    public Collection<Picture> getPictures() {
        return pictures;
    }

    public void setPictures(Collection<Picture> pictures) {
        this.pictures = pictures;
    }

    @NotNull
    @OneToMany(mappedBy = "article",cascade = CascadeType.REMOVE)
    public Collection<FollowUp> getFollowUps() {
        return followUps;
    }

    public void setFollowUps(Collection<FollowUp> followUps) {
        this.followUps = followUps;
    }
}

