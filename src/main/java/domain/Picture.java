package domain;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Access(AccessType.PROPERTY)
public class Picture extends DomainEntity {

    // Atributos ----
    private String image;

    // Constructor ----

    public Picture() {
        super();
    }

    @NotNull
    @NotBlank
    @URL
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    private Newspaper newspaper;
    private Article article;
    private FollowUp followUp;

    @OneToOne
    public Newspaper getNewspaper() {
        return newspaper;
    }

    public void setNewspaper(Newspaper newspaper) {
        this.newspaper = newspaper;
    }

    @ManyToOne
    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    @ManyToOne
    public FollowUp getFollowUp() {
        return followUp;
    }

    public void setFollowUp(FollowUp followUp) {
        this.followUp = followUp;
    }
}
