package services;

import domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import repositories.SubscriptionRepository;
import forms.ArticleForm;
import forms.SubscriptionForm;

@Service
@Transactional
public class SubscriptionService {
    // Managed repository -----------------------------------------------------
    @Autowired
    private SubscriptionRepository subscriptionRepository;
    
    @Autowired
    private ActorService actorService;
    
    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private NewspaperService newspaperService;

    @Autowired
    private VolumeService volumeService;
    
// Suporting repository --------------------------------------------------

    // Constructors -----------------------------------------------------------
    public SubscriptionService() {
        super();
    }

    // Simple CRUD methods ----------------------------------------------------
    public Subscription create() {
        Subscription result;
        result = new Subscription();
        Customer customer = (Customer) actorService.findByPrincipal();
        result.setCustomer(customer);
        return result;
    }

    public Collection<Subscription> findAll() {
        Collection<Subscription> result;
        Assert.notNull(subscriptionRepository);
        result = subscriptionRepository.findAll();
        Assert.notNull(result);
        return result;
    }

    public Subscription findOne(int subscriptionId) {
        Subscription result;
        result = subscriptionRepository.findOne(subscriptionId);
        return result;
    }

    public Subscription save(Subscription subscription) {
        Assert.notNull(subscription);
        Subscription result;
        result = subscriptionRepository.save(subscription);
        return result;
    }

    public void delete(Subscription subscription) {
        Assert.notNull(subscription);
        Assert.isTrue(subscription.getId() != 0);
        subscriptionRepository.delete(subscription);
    }
    
    public Subscription reconstruct(SubscriptionForm subscriptionForm){
    	Subscription res = this.create();
    	CreditCard creditcard = new CreditCard();
    	creditcard.setBrand(subscriptionForm.getBrand());
    	creditcard.setCvv(subscriptionForm.getCvv());
    	creditcard.setExpiryMonth(subscriptionForm.getExpiryMonth());
    	creditcard.setExpiryYear(subscriptionForm.getExpiryYear());
    	creditcard.setHolderName(subscriptionForm.getHolderName());
    	creditcard.setNumber(subscriptionForm.getNumber());
        res.setCreditcard(creditcard);
        Calendar time = Calendar.getInstance();
        Integer mes = time.get(Calendar.MONTH) + 1 ;
        Integer anio = time.get(Calendar.YEAR);
        Assert.isTrue((subscriptionForm.getExpiryYear().equals(anio) && (subscriptionForm.getExpiryMonth().compareTo(mes) >= 1) ) || (subscriptionForm.getExpiryYear() > anio),"creditcard.date.error");
        res.setCustomer(customerService.findOne(actorService.findByPrincipal().getId()));
        if (subscriptionForm.getNewspaperId() != null) {
            res.setNewspaper(newspaperService.findOne(subscriptionForm.getNewspaperId()));
        } else if (subscriptionForm.getVolumeId() != null){
            res.setVolume(volumeService.findOne(subscriptionForm.getVolumeId()));
        }

        return res;
    }

	public Integer findAllByCustomerAndNewspaper(Integer newspaperId) {
		try{
			Actor actor = actorService.findByPrincipal();
			return subscriptionRepository.findAllByCustomerAndNewspaper(actor.getId(), newspaperId).size();
		} catch(Exception e){
			return 0;
		}
	}
    
    

// Other business methods -------------------------------------------------

    public void subscribeNewspaperByVolume(Subscription subscription){
        Volume volume = subscription.getVolume();
        for (Newspaper n : volume.getNewspapers()){
            Subscription s = create();
            s.setNewspaper(n);
            s.setCreditcard(subscription.getCreditcard());
            save(s);
        }
    }
}
