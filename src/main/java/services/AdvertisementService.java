package services;

import domain.Agent;
import domain.CreditCard;
import forms.AdvertisementForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import repositories.AdvertisementRepository;
import domain.Advertisement;

@Service
@Transactional
public class AdvertisementService {
    // Managed repository -----------------------------------------------------
    @Autowired
    private AdvertisementRepository advertisementRepository;
    // Suporting repository --------------------------------------------------
    @Autowired
    private ActorService actorService;

    // Constructors -----------------------------------------------------------
    public AdvertisementService() {
        super();
    }

    // Simple CRUD methods ----------------------------------------------------
    public Advertisement create() {
        Advertisement result;
        result = new Advertisement();
        result.setAgent((Agent) actorService.findByPrincipal());
        return result;
    }

    public Collection<Advertisement> findAll() {
        Collection<Advertisement> result;
        Assert.notNull(advertisementRepository);
        result = advertisementRepository.findAll();
        Assert.notNull(result);
        return result;
    }

    public Advertisement findOne(int advertisementId) {
        Advertisement result;
        result = advertisementRepository.findOne(advertisementId);
        return result;
    }

    public Advertisement save(Advertisement advertisement) {
        Assert.notNull(advertisement);
        Advertisement result;
        result = advertisementRepository.save(advertisement);
        return result;
    }

    public void delete(Advertisement advertisement) {
        Assert.notNull(advertisement);
        Assert.isTrue(advertisement.getId() != 0);
        advertisementRepository.delete(advertisement);
    }

    public Advertisement reconstruct(AdvertisementForm advertisementForm){
        Advertisement res = this.create();
        res.setTitle(advertisementForm.getTitle());
        res.setUrlBanner(advertisementForm.getUrlBanner());
        res.setUrlPage(advertisementForm.getUrlPage());
        res.setNewspaper(advertisementForm.getNewspaper());
        Calendar time = Calendar.getInstance();
        Integer mes = time.get(Calendar.MONTH) + 1 ;
        Integer anio = time.get(Calendar.YEAR);
        Assert.isTrue((advertisementForm.getCreditCard().getExpiryYear().equals(anio) && (advertisementForm.getCreditCard().getExpiryMonth().compareTo(mes) >= 1) ) || (advertisementForm.getCreditCard().getExpiryYear() > anio),"creditcard.date.error");
        res.setCreditcard(advertisementForm.getCreditCard());
        return res;
    }

	public Collection<Advertisement> containsTabooWord() {
		return advertisementRepository.containsTabooWord();
	}

// Other business methods -------------------------------------------------
}
