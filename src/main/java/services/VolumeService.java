package services;

import domain.Newspaper;
import domain.User;
import forms.AddNewspaperForm;
import forms.VolumeForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.Collection;
import java.util.Date;

import repositories.VolumeRepository;
import domain.Volume;

@Service
@Transactional
public class VolumeService {
    // Managed repository -----------------------------------------------------
    @Autowired
    private VolumeRepository volumeRepository;
// Suporting repository --------------------------------------------------

    @Autowired
    private ActorService actorService;

    @Autowired
    private NewspaperService newspaperService;

    // Constructors -----------------------------------------------------------
    public VolumeService() {
        super();
    }

    // Simple CRUD methods ----------------------------------------------------
    public Volume create() {
        Volume result;
        result = new Volume();
        result.setUser((User) actorService.findByPrincipal());
        return result;
    }

    public Collection<Volume> findAll() {
        Collection<Volume> result;
        Assert.notNull(volumeRepository);
        result = volumeRepository.findAll();
        Assert.notNull(result);
        return result;
    }

    public Volume findOne(int volumeId) {
        Volume result;
        result = volumeRepository.findOne(volumeId);
        return result;
    }

    public Volume save(Volume volume) {
        Assert.notNull(volume);
        Volume result;
        result = volumeRepository.save(volume);
        return result;
    }

    public void delete(Volume volume) {
        Assert.notNull(volume);
        Assert.isTrue(volume.getId() != 0);
        volumeRepository.delete(volume);
    }

// Other business methods -------------------------------------------------

    public Volume reconstruct(VolumeForm volumeForm){
        Volume res;
        if (volumeForm.getVolumeId() == 0){
            res = create();
        } else {
            res = findOne(volumeForm.getVolumeId());
        }

        res.setDescription(volumeForm.getDescription());
        res.setTitle(volumeForm.getTitle());
        res.setYear(volumeForm.getYear());

        return res;
    }

    public VolumeForm construct(Integer volumeId){
        VolumeForm res = new VolumeForm();
        Volume volume = findOne(volumeId);
        res.setVolumeId(volume.getId());
        res.setDescription(volume.getDescription());
        res.setTitle(volume.getTitle());
        res.setYear(volume.getYear());

        return res;
    }

    public Volume isMine(Integer volumeId){
        Volume res = findOne(volumeId);
        User user = (User) actorService.findByPrincipal();
        Assert.isTrue(res.getUser().getId() == user.getId());
        return res;
    }

    public Collection<Volume> findByUser(){
        User user = (User) actorService.findByPrincipal();
        Collection<Volume> res = volumeRepository.findByUser(user.getId());
        Assert.notNull(res);
        return res;
    }

    public void addNewspaper(AddNewspaperForm addNewspaperForm){
        Volume volume = isMine(addNewspaperForm.getVolumeId());
        Newspaper newspaper = addNewspaperForm.getNewspaper();
        Assert.isTrue(newspaper.getDraft() == false);
        if (!volume.getNewspapers().contains(newspaper)){
            volume.getNewspapers().add(newspaper);
            save(volume);
        }
    }

    public void quitNewspaper(Integer volumenId,Integer newspaperId){
        Volume volume = isMine(volumenId);
        Newspaper newspaper = newspaperService.findOne(newspaperId);
        if (volume.getNewspapers().contains(newspaper)){
            volume.getNewspapers().remove(newspaper);
        }
        save(volume);
    }

}
