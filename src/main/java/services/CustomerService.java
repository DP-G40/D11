package services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.Collection;
import java.util.Date;

import repositories.CustomerRepository;
import security.Authority;
import security.UserAccount;
import domain.Customer;
import domain.User;
import forms.RegisterForm;

@Service
@Transactional
public class CustomerService {
    // Managed repository -----------------------------------------------------
    @Autowired
    private CustomerRepository customerRepository;
    
    @Autowired
    private ActorService actorService;

    @Autowired
    private FolderService folderService;
// Suporting repository --------------------------------------------------

    // Constructors -----------------------------------------------------------
    public CustomerService() {
        super();
    }

    // Simple CRUD methods ----------------------------------------------------
    public Customer create() {
        Customer result;
        result = new Customer();
        return result;
    }

    public Collection<Customer> findAll() {
        Collection<Customer> result;
        Assert.notNull(customerRepository);
        result = customerRepository.findAll();
        Assert.notNull(result);
        return result;
    }

    public Customer findOne(int customerId) {
        Customer result;
        result = customerRepository.findOne(customerId);
        return result;
    }

    public Customer save(Customer customer) {
        Assert.notNull(customer);
        Customer result;
        result = customerRepository.save(customer);
        return result;
    }

    public void delete(Customer customer) {
        Assert.notNull(customer);
        Assert.isTrue(customer.getId() != 0);
        customerRepository.delete(customer);
    }

// Other business methods -------------------------------------------------
    
    public Customer reconstruct(RegisterForm registerForm){
        Customer res = this.create();
        Assert.isTrue(registerForm.getPassword().equals(registerForm.getRepeatPassword()),"register.error.password");
        Assert.isNull(actorService.getByUserName(registerForm.getUserName()),"register.error.username");
        Md5PasswordEncoder md5 = new Md5PasswordEncoder();
        UserAccount userAccount = new UserAccount();
        Authority a = new Authority();
        a.setAuthority(Authority.CUSTOMER);
        userAccount.getAuthorities().add(a);
        userAccount.setPassword(md5.encodePassword(registerForm.getPassword(),null));
        userAccount.setUsername(registerForm.getUserName());
        res.setUserAccount(userAccount);
        res.setName(registerForm.getName());
        res.setSurname(registerForm.getSurname());
        res.setPhoneNumber(registerForm.getPhone());
        res.setEmail(registerForm.getEmail());
        res.setDirection(registerForm.getAddress());

        folderService.addDefaultFolders(res);
        return res;

    }
}
