package services;

import domain.User;
import forms.ChirpForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.Collection;
import java.util.Date;

import repositories.ChirpRepository;
import domain.Chirp;

@Service
@Transactional
public class ChirpService {
    // Managed repository -----------------------------------------------------
    @Autowired
    private ChirpRepository chirpRepository;

    @Autowired
    private ActorService actorService;
// Suporting repository --------------------------------------------------

    // Constructors -----------------------------------------------------------
    public ChirpService() {
        super();
    }

    // Simple CRUD methods ----------------------------------------------------
    public Chirp create() {
        Chirp result;
        result = new Chirp();
        User user = (User) actorService.findByPrincipal();
        result.setUser(user);
        return result;
    }

    public Collection<Chirp> findAll() {
        Collection<Chirp> result;
        Assert.notNull(chirpRepository);
        result = chirpRepository.findAll();
        Assert.notNull(result);
        return result;
    }

    public Chirp findOne(int chirpId) {
        Chirp result;
        result = chirpRepository.findOne(chirpId);
        return result;
    }

    public Chirp save(Chirp chirp) {
        Assert.notNull(chirp);
        Chirp result;
        result = chirpRepository.save(chirp);
        return result;
    }

    public void delete(Chirp chirp) {
        Assert.notNull(chirp);
        Assert.isTrue(chirp.getId() != 0);
        chirpRepository.delete(chirp);
    }

	public Collection<Chirp> containsTabooWord() {
		return chirpRepository.containsTabooWord();
	}
    
    

// Other business methods -------------------------------------------------



    public Collection<Chirp> findAllByUser(int idUser){
            return chirpRepository.findAllByUser(idUser);
    }

    public Chirp reconstruct(ChirpForm chirpForm){
        Chirp res;
        if (chirpForm.getChirpId() == 0){
            res = create();
        }else {
            res = findOne(chirpForm.getChirpId());
        }
        res.setTitle(chirpForm.getTitle());
        res.setDescription(chirpForm.getDescription());
        return res;

    }


    public Collection<Chirp> findChirpByFollowing(Collection<User> a){
        return chirpRepository.findChirpByFollowing(a);
    }



}
