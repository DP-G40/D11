package services;

import domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import repositories.FolderRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
@Transactional
public class FolderService {

    @Autowired
    private FolderRepository folderRepository;
    @Autowired
    private ActorService actorService;
    @Autowired
    private TabooWordService spamService;

    @Autowired
    private  MessageService messageService;


    public FolderService(){
        super();
    }

    public Folder create(final Actor actor) {
        final Collection<Message> messages = new ArrayList<Message>();
        final Folder folder = new Folder();

        folder.setActor(actor);
        folder.setMessages(messages);
        folder.setName("");

        return folder;
    }

    public Collection<Folder> findAll(){
        Collection<Folder> result;

        result = folderRepository.findAll();

        Assert.notNull(result);

        return result;
    }

    public Folder findOne(final Integer folderId){
        Assert.isTrue(folderId != 0);

        Folder result;

        result = this.folderRepository.findOne(folderId);
        Assert.notNull(result);

        return result;
    }

    public Folder findOneEdit(final int folderId) {
        Assert.isTrue(folderId != 0);

        Folder result;

        result = this.folderRepository.findOne(folderId);

        this.checkPrincipalActor(result);

        Assert.notNull(result);
        Assert.isTrue(!result.getSystem());

        return result;
    }


    public Folder save(final Folder folder){
        Assert.notNull(folder);

        //CONCURRENCY CHECK
        if (folder.getId() != 0) {
            final Folder folderCheck = this.folderRepository.findOne(folder.getId());
            Assert.isTrue(folder.getVersion() == folderCheck.getVersion());
        }
        if (folder.getId() != 0)
            this.checkPrincipalActor(folder);

        Folder result;

        result = this.folderRepository.save(folder);

        return result;
    }

    public void delete(final Folder folder){
        Assert.notNull(folder);
        Assert.isTrue(!folder.getSystem());

        this.checkPrincipalActor(folder);

        this.folderRepository.delete(folder);
    }

    public void addDefaultFolders(Actor actor) {
        Assert.notNull(actor);

        Folder f1, f2, f3, f4, f5;
        List<Folder> folders = new ArrayList<Folder>();

        f1 = new Folder();
        f1.setName("in box");
        f1.setActor(actor);
        f1.setSystem(true);
        folders.add(f1);

        f2 = new Folder();
        f2.setName("out box");
        f2.setActor(actor);
        f2.setSystem(true);
        folders.add(f2);

        f3 = new Folder();
        f3.setName("trash box");
        f3.setActor(actor);
        f3.setSystem(true);
        folders.add(f3);

        f4 = new Folder();
        f4.setName("spam box");
        f4.setActor(actor);
        f4.setSystem(true);
        folders.add(f4);

        f5 = new Folder();
        f5.setName("notification box");
        f5.setActor(actor);
        f5.setSystem(true);
        folders.add(f5);

        actor.setFolders(folders);

    }


    public Collection<Folder> findFoldersByPrincipal() {
        Collection<Folder> result;
        Actor actor;

        actor = this.actorService.getPrincipal();
        Assert.notNull(actor, "EL actor es nulo");
        result = this.folderRepository.findFoldersByUserAccount(actor.getUserAccount().getId());
        Assert.notNull(result, "La coleccion de folders es nula");

        this.checkPrincipalActorFolders(result);

        return result;
    }

    public Folder findSystemFolder(final String nameFolder) {
        Assert.notNull(nameFolder, "La carpeta es nula");

        Actor actor;
        Folder folder;

        actor = this.actorService.getPrincipal();
        folder = this.folderRepository.findSystemFolder(nameFolder, actor.getId());
        Assert.notNull(folder, "La carpeta es nula");

        return folder;
    }

    private Folder findSystemFolderByActor(final String nameFolder, final int actorId) {
        Assert.notNull(nameFolder);
        Folder folder;

        folder = this.folderRepository.findSystemFolder(nameFolder, actorId);
        Assert.notNull(folder);
        return folder;
    }

    public Folder getFolderAndCheckSpam(final Message message, final Actor recipient, Boolean broadcast) {
        Assert.notNull(message);
        Folder folder;
        Collection<TabooWord> spamList;
        boolean isSpam;

        spamList = this.spamService.findAll();

        isSpam = false;

        for (final TabooWord sp : spamList)
            if (message.getBody().toLowerCase().contains(sp.getWord().toLowerCase()) || message.getSubject().toLowerCase().contains(sp.getWord().toLowerCase())) {
                isSpam = true;
                break;
            }

        if (isSpam) {
            folder = this.findSystemFolderByActor("spam box", recipient.getId());
            message.setSpam(true);
            Actor enviador=null;

        }else if(broadcast){
            folder = this.findSystemFolderByActor("notification box", recipient.getId());
        }else {
            folder = this.findSystemFolderByActor("in box", recipient.getId());
        }
        return folder;
    }



    public void checkPrincipalActor(final Folder folder) {
        Assert.notNull(folder);

        Actor actor;

        actor = this.actorService.getPrincipal();

        Assert.isTrue(actor.getId() == folder.getActor().getId());
    }





    public Collection<Folder> findFoldersToMoveByPrincipal() {
        Collection<Folder> result;
        Actor principal;

        principal = this.actorService.getPrincipal();
        result = this.folderRepository.findFoldersToMoveByUserAccount(principal.getUserAccount().getId());
        Assert.notNull(result);

        return result;
    }

    private void checkPrincipalActorFolders(final Collection<Folder> folders) {
        Assert.notNull(folders);

        for (final Folder f : folders)
            this.checkPrincipalActor(f);
    }



    public List<Folder> getFoldersByActor(final int actorId) {
        Assert.notNull(actorId);
        final List<Folder> result = (List<Folder>) this.folderRepository.getFoldersByActor(actorId);

        return result;
    }
}
