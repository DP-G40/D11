package services;

import domain.Advertisement;
import domain.Newspaper;
import domain.User;
import forms.ArticleForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import repositories.ArticleRepository;
import domain.Article;

@Service
@Transactional
public class ArticleService {
    // Managed repository -----------------------------------------------------
    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    ActorService actorService;

    @Autowired
    private NewspaperService newspaperService;
// Suporting repository --------------------------------------------------

    // Constructors -----------------------------------------------------------
    public ArticleService() {
        super();
    }

    // Simple CRUD methods ----------------------------------------------------
    public Article create() {
        Article result;
        result = new Article();
        result.setUser((User) actorService.findByPrincipal());
        return result;
    }

    public Collection<Article> findAll() {
        Collection<Article> result;
        Assert.notNull(articleRepository);
        result = articleRepository.findAll();
        Assert.notNull(result);
        return result;
    }

    public Article findOne(int articleId) {
        Article result;
        result = articleRepository.findOne(articleId);
        return result;
    }

    public Collection<Article> containsTabooWord() {
		return articleRepository.containsTabooWord();
	}

	public Article save(Article article) {
        Assert.notNull(article);
        Article result;
        result = articleRepository.save(article);
        return result;
    }

    public void delete(Article article) {
        Assert.notNull(article);
        Assert.isTrue(article.getId() != 0);
        articleRepository.delete(article);
    }

	public Collection<Article> findByNewspaper(Integer newspaperId) {
		return articleRepository.findAllNoDraft(newspaperId);
	}

	public Collection<Article> findByKeyword(String keyword) {
		return articleRepository.findByKeyword(keyword);
	}

	public Collection<Article> findAllNoDraftWithourNewspaper() {
		return articleRepository.findAllNoDraftWithourNewspaper();
	}
	
	

// Other business methods -------------------------------------------------


    public Article reconstruct(ArticleForm articleForm){
        Article res;
        if (articleForm.getArticleId() == 0){
            res = create();
        }else {
            res = findOne(articleForm.getArticleId());
        }
        res.setBody(articleForm.getBody());
        res.setNewspaper(articleForm.getNewspaper());
        res.setSummary(articleForm.getSummary());
        res.setTitle(articleForm.getTitle());

        return res;
    }

    public void isMine(Integer articleId){
        User user = (User) actorService.findByPrincipal();
        Article article = findOne(articleId);
        Assert.isTrue(user.getId() == article.getUser().getId());
    }


    public ArticleForm construct( Article article){
        ArticleForm res = new ArticleForm();
        res.setArticleId(article.getId());
        res.setBody(article.getBody());
        res.setNewspaper(article.getNewspaper());
        res.setSummary(article.getSummary());
        res.setTitle(article.getTitle());

        return res;
    }

    public String getRandomAnnouncement(Article a){
        String banner;
        Advertisement res;
        Newspaper n =a.getNewspaper();
        try {
            List<Advertisement> aux = (List<Advertisement>) n.getAdvertisements();
            Random r = new Random();
            Integer indice = r.nextInt((aux.size()-1 - 0) + 1) + 0;
            res = aux.get(indice);
            banner = res.getUrlBanner();
        } catch (Throwable oops){
            banner="";
        }


        return banner;
    }
    
    
}
