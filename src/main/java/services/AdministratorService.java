package services;

import domain.Administrator;
import domain.Newspaper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.AdministratorRepository;

import java.util.Collection;
import java.util.List;

@Service
@Transactional
public class AdministratorService {
    // Managed repository -----------------------------------------------------
    @Autowired
    private AdministratorRepository administratorRepository;
// Suporting repository --------------------------------------------------

    // Constructors -----------------------------------------------------------
    public AdministratorService() {
        super();
    }

    // Simple CRUD methods ----------------------------------------------------
    public Administrator create() {
        Administrator result;
        result = new Administrator();
        return result;
    }

    public Collection<Administrator> findAll() {
        Collection<Administrator> result;
        Assert.notNull(administratorRepository);
        result = administratorRepository.findAll();
        Assert.notNull(result);
        return result;
    }

    public Administrator findOne(int administratorId) {
        Administrator result;
        result = administratorRepository.findOne(administratorId);
        return result;
    }

    public Administrator save(Administrator administrator) {
        Assert.notNull(administrator);
        Administrator result;
        result = administratorRepository.save(administrator);
        return result;
    }

    public void delete(Administrator administrator) {
        Assert.notNull(administrator);
        Assert.isTrue(administrator.getId() != 0);
        administratorRepository.delete(administrator);
    }

	public List<Object> Q1() {
		return administratorRepository.Q1();
	}

	public List<Object> Q2() {
		return administratorRepository.Q2();
	}

	public List<Object> Q3() {
		return administratorRepository.Q3();
	}

	public List<Newspaper> Q4() {
		return administratorRepository.Q4();
	}

	public List<Newspaper> Q5() {
		return administratorRepository.Q5();
	}

	public Double Q6() {
		return administratorRepository.Q6();
	}

	public Double Q7() {
		return administratorRepository.Q7();
	}

	public Double Q8() {
		return administratorRepository.Q8();
	}

	public Double Q9() {
		return administratorRepository.Q9();
	}

	public Double Q10() {
		return administratorRepository.Q10();
	}

	public List<Object> Q11() {
		return administratorRepository.Q11();
	}

	public Double Q12() {
		return administratorRepository.Q12();
	}

	public Double Q13() {
		return administratorRepository.Q13();
	}

	public Double Q14() {
		return administratorRepository.Q14();
	}

	public Double Q15() {
		return administratorRepository.Q15();
	}

	public Double Q16() {
		return administratorRepository.Q16();
	}

	public Double Q17() {
		return administratorRepository.Q17();
	}
	
	public Double Q18() {
		return administratorRepository.Q18();
	}

	public Double Q19() {
		return administratorRepository.Q19();
	}

	public Double Q20() {
		return administratorRepository.Q20();
	}

	public Double Q21() {
		return administratorRepository.Q21();
	}
    
}
