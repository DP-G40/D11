package services;

import domain.Actor;
import forms.ActorForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.ActorRepository;
import security.LoginService;
import security.UserAccount;

import java.util.Collection;

@Service
@Transactional
public class ActorService {
    // Managed repository -----------------------------------------------------
    @Autowired
    private ActorRepository actorRepository;
// Suporting repository --------------------------------------------------

    // Constructors -----------------------------------------------------------
    public ActorService() {
        super();
    }

    // Simple CRUD methods ----------------------------------------------------

    public Collection<Actor> findAll() {
        Collection<Actor> result;
        Assert.notNull(actorRepository);
        result = actorRepository.findAll();
        Assert.notNull(result);
        return result;
    }

    public Actor findOne(int actorId) {
        Actor result;
        result = actorRepository.findOne(actorId);
        return result;
    }

    public Actor save(Actor actor) {
        Assert.notNull(actor);
        Actor result;
        result = actorRepository.save(actor);
        return result;
    }

    public Actor save(final ActorForm actorform) {
        Actor result = this.getPrincipal();
        Assert.notNull(result);

        result.setEmail(actorform.getEmail());
        result.setName(actorform.getName());
        result.setPhoneNumber(actorform.getPhoneNumber());
        result.setSurname(actorform.getSurname());
        result.setDirection(actorform.getDirection());

        Assert.notNull(result);
        return result;

    }


    public void delete(Actor actor) {
        Assert.notNull(actor);
        Assert.isTrue(actor.getId() != 0);
        actorRepository.delete(actor);
    }
    
    public Actor findByPrincipal(){
		Actor result;
		UserAccount userAccount;
		
		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);
		result = findByUserAccount(userAccount);
		Assert.notNull(result);

		return result;
	}
    
    public Actor findByUserAccount(UserAccount userAccount) {
		Assert.notNull(userAccount);

		Actor result;

		result = actorRepository.getPrincipal(userAccount.getId());		

		return result;
	}

    public Actor findByUserAccount(Integer id) {
        Assert.notNull(id);

        Actor result;

        result = actorRepository.getPrincipal(id);

        return result;
    }

// Other business methods -------------------------------------------------


    public Actor getPrincipal() {

        Actor result = actorRepository.getPrincipal(LoginService.getPrincipal()
                .getId());
        Assert.notNull(result);

        return result;
    }


    public Actor getByUserName(String username) {
        return actorRepository.getByUserName(username);
    }



}
