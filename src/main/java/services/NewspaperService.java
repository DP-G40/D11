package services;

import com.sun.org.apache.xpath.internal.operations.Bool;
import domain.Newspaper;
import domain.Picture;
import domain.User;
import domain.Volume;
import forms.NewspaperForm;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.NewspaperRepository;

import java.util.Collection;
import java.util.Date;

@Service
@Transactional
public class NewspaperService {
    // Managed repository -----------------------------------------------------
    @Autowired
    private NewspaperRepository newspaperRepository;

    @Autowired
    private ActorService actorService;

    @Autowired
    private PictureService pictureService;

    @Autowired
    private VolumeService volumeService;
// Suporting service --------------------------------------------------


    // Constructors -----------------------------------------------------------
    public NewspaperService() {
        super();
    }

    // Simple CRUD methods ----------------------------------------------------
    public Newspaper create() {
        Newspaper result;
        result = new Newspaper();
        result.setUser((User) actorService.findByPrincipal());
        return result;
    }

    public Collection<Newspaper> findAll() {
        Collection<Newspaper> result;
        Assert.notNull(newspaperRepository);
        result = newspaperRepository.findAll();
        Assert.notNull(result);
        return result;
    }

    public Newspaper findOne(int newspaperId) {
        Newspaper result;
        result = newspaperRepository.findOne(newspaperId);
        return result;
    }

    public Collection<Newspaper> containsTabooWord() {
		return newspaperRepository.containsTabooWord();
	}

	public Newspaper save(Newspaper newspaper) {
        Assert.notNull(newspaper);
        Newspaper result;
        result = newspaperRepository.save(newspaper);
        return result;
    }

    public void delete(Newspaper newspaper) {
        Assert.notNull(newspaper);
        Assert.isTrue(newspaper.getId() != 0);
        newspaperRepository.delete(newspaper);
    }

    public Newspaper reconstruct(NewspaperForm newspaperForm){
        Newspaper res;
        if (newspaperForm.getNewspaperId() == 0){
            res = this.create();
        } else {
            res = findOne(newspaperForm.getNewspaperId());
        }

        res.setTitle(newspaperForm.getTitle());
        res.setDescription(newspaperForm.getDescription());
        return res;
    }

    public Collection<Newspaper> findAllNoDraft() {
        return newspaperRepository.findAllNoDraft();
    }

    public Collection<Newspaper> findByKeyword(String keyword) {
        return newspaperRepository.findByKeyword(keyword);
    }

    public Collection<Newspaper> findAllByUser(Integer id) {
        return newspaperRepository.findAllByUser(id);
    }

    public Collection<Newspaper> findToPublish(){
        User user  = (User) actorService.findByPrincipal();
        Integer userId = user.getId();
        return this.newspaperRepository.findToPublish(userId);
    }

    public Newspaper publish(Newspaper newspaper, Boolean privateFlag){
        newspaper.setPrivateFlag(privateFlag);
        newspaper.setDraft(false);
        newspaper.setPublicationDate(new Date());
        newspaper = save(newspaper);
        return newspaper;
    }

    public Collection<Newspaper> findByUserAndDraft(){
        User user  = (User) actorService.findByPrincipal();
        Integer userId = user.getId();
        return this.newspaperRepository.findByUserAndDraft(userId);
    }

    public void isMine(Newspaper newspaper,Boolean option){
        User user = (User) actorService.findByPrincipal();
        if (option) {
            Assert.isTrue(user.getId() == newspaper.getUser().getId());
        } else {
            Assert.isTrue(user.getId() == newspaper.getUser().getId() && newspaper.getDraft() == true);
        }
    }



    public NewspaperForm construct(Integer newspaperId){
        Newspaper newspaper = findOne(newspaperId);
        isMine(newspaper,false);
        NewspaperForm res = new NewspaperForm();
        res.setNewspaperId(newspaperId);
        res.setDescription(newspaper.getDescription());
        res.setTitle(newspaper.getTitle());
        String url = (newspaper.getPicture() == null)? null:newspaper.getPicture().getImage();
        res.setPicture(url);
        return res;
    }

	public Collection<Newspaper> findAllWithNoAdd() {
		return this.newspaperRepository.findAllWithNoAdd();
	}

	public Collection<Newspaper> findAllWithAdd() {
		return this.newspaperRepository.findAllWithAdd();
	}

// Other business methods -------------------------------------------------


    public Collection<Newspaper> findNotInVolume(Integer volumeId){
        Volume volumen = volumeService.findOne(volumeId);
        Collection<Newspaper> res = newspaperRepository.findAllNoDraft();
        res.removeAll(volumen.getNewspapers());
        Assert.notNull(res);
        return res;
    }
}
