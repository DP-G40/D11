package services;

import domain.Agent;
import domain.Customer;
import domain.Folder;
import forms.RegisterForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import repositories.AgentRepository;
import security.Authority;
import security.UserAccount;

import java.util.Collection;

@Service
@Transactional
public class AgentService {
    // Managed repository -----------------------------------------------------
    @Autowired
    private AgentRepository agentRepository;

    @Autowired
    private ActorService actorService;

    @Autowired
    private FolderService folderService;
// Suporting repository --------------------------------------------------

    // Constructors -----------------------------------------------------------
    public AgentService() {
        super();
    }

    // Simple CRUD methods ----------------------------------------------------
    public Agent create() {
        Agent result;
        result = new Agent();
        return result;
    }

    public Collection<Agent> findAll() {
        Collection<Agent> result;
        Assert.notNull(agentRepository);
        result = agentRepository.findAll();
        Assert.notNull(result);
        return result;
    }

    public Agent findOne(int agentId) {
        Agent result;
        result = agentRepository.findOne(agentId);
        return result;
    }

    public Agent save(Agent agent) {
        Assert.notNull(agent);
        Agent result;
        result = agentRepository.save(agent);
        return result;
    }

    public void delete(Agent agent) {
        Assert.notNull(agent);
        Assert.isTrue(agent.getId() != 0);
        agentRepository.delete(agent);
    }

// Other business methods -------------------------------------------------


    public Agent reconstruct(RegisterForm registerForm){
        Agent res = this.create();
        Assert.isTrue(registerForm.getPassword().equals(registerForm.getRepeatPassword()),"register.error.password");
        Assert.isNull(actorService.getByUserName(registerForm.getUserName()),"register.error.username");
        Md5PasswordEncoder md5 = new Md5PasswordEncoder();
        UserAccount userAccount = new UserAccount();
        Authority a = new Authority();
        a.setAuthority(Authority.AGENT);
        userAccount.getAuthorities().add(a);
        userAccount.setPassword(md5.encodePassword(registerForm.getPassword(),null));
        userAccount.setUsername(registerForm.getUserName());
        res.setUserAccount(userAccount);
        res.setName(registerForm.getName());
        res.setSurname(registerForm.getSurname());
        res.setPhoneNumber(registerForm.getPhone());
        res.setEmail(registerForm.getEmail());
        res.setDirection(registerForm.getAddress());

        folderService.addDefaultFolders(res);
        return res;

    }
}
