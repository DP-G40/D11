package services;

import domain.Article;
import domain.Newspaper;
import domain.User;
import forms.NewspaperForm;
import forms.PictureForm;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.Collection;
import java.util.Date;

import repositories.PictureRepository;
import domain.Picture;

@Service
@Transactional
public class PictureService {
    // Managed repository -----------------------------------------------------
    @Autowired
    private PictureRepository pictureRepository;

    @Autowired
    private ArticleService articleService;

    @Autowired
    private FollowUpService followUpService;
// Suporting repository --------------------------------------------------

    // Constructors -----------------------------------------------------------
    public PictureService() {
        super();
    }

    // Simple CRUD methods ----------------------------------------------------
    public Picture create() {
        Picture result;
        result = new Picture();
        return result;
    }

    public Collection<Picture> findAll() {
        Collection<Picture> result;
        Assert.notNull(pictureRepository);
        result = pictureRepository.findAll();
        Assert.notNull(result);
        return result;
    }

    public Picture findOne(int pictureId) {
        Picture result;
        result = pictureRepository.findOne(pictureId);
        return result;
    }

    public Picture save(Picture picture) {
        Assert.notNull(picture);
        Picture result;
        result = pictureRepository.save(picture);
        return result;
    }

    public void delete(Picture picture) {
        Assert.notNull(picture);
        Assert.isTrue(picture.getId() != 0);
        pictureRepository.delete(picture);
    }

// Other business methods -------------------------------------------------

    public void reconstruct(Newspaper newspaper, NewspaperForm newspaperForm){
        if (newspaper.getPicture() == null && StringUtils.isNotBlank(newspaperForm.getPicture())) {
            Picture picture = new Picture();
            picture.setImage(newspaperForm.getPicture());
            picture.setNewspaper(newspaper);
            save(picture);
        } else if (newspaper.getPicture() != null && StringUtils.isNotBlank(newspaperForm.getPicture())){
            newspaper.getPicture().setImage(newspaperForm.getPicture());
            save(newspaper.getPicture());
        }
    }

    public void reconstruct(PictureForm pictureForm){
        Picture res = new Picture();
        articleService.isMine(pictureForm.getArticleId());
        res.setImage(pictureForm.getImage());
        res.setArticle(articleService.findOne(pictureForm.getArticleId()));
        save(res);
    }

    public void reconstruct(PictureForm pictureForm,Boolean option){
        Picture res = new Picture();
        followUpService.isMine(pictureForm.getArticleId());
        res.setImage(pictureForm.getImage());
        res.setFollowUp(followUpService.findOne(pictureForm.getArticleId()));
        save(res);
    }
}
