package services;

import domain.Actor;
import domain.Administrator;
import domain.Folder;
import domain.Message;
import forms.BroadcastForm;
import forms.MessageForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import repositories.MessageRepository;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.Date;
import forms.MessageMoveForm;


@Service
@Transactional
public class MessageService {

    @Autowired
    private MessageRepository messageRepository;
    @Autowired
    private ActorService actorService;
    @Autowired
    private FolderService folderService;
    @Autowired
    private TabooWordService spamService;

    public MessageService() {
        super();
    }

    public Message create() {
        Message result;
        Actor sender;
        Folder folder;

        sender = this.actorService.getPrincipal();
        folder = this.folderService.findSystemFolder("out box");

        result = new Message();
        result.setSender(sender);
        result.setMoment(new Date(System.currentTimeMillis()));
        result.setFolder(folder);

        return result;
    }


    public Collection<Message> findAll() {
        Collection<Message> result;

        result = messageRepository.findAll();

        Assert.notNull(result);

        return result;
    }

    public Message findOne(final Integer messageId) {
        Message result;

        Assert.notNull(messageId);

        result = messageRepository.findOne(messageId);

        Assert.notNull(result);
        return result;
    }


    public Message saveToSend(final Message message, Boolean broadcast) {
        Assert.notNull(message);
        Message result;

        result = this.save(message);
        Message copy;
        Actor receiver;
        Folder folder;

        copy = message.clone();
        receiver = message.getRecipient();
        folder = this.folderService.getFolderAndCheckSpam(copy, receiver, broadcast);

        copy.setFolder(folder);
        this.save(copy);

        return result;
    }

    public void moveMessage(Message message, Integer folderId){
        Actor actor =  actorService.getPrincipal();
        Folder folder = folderService.findOne(folderId);
        Assert.isTrue(folder.getActor().getId()==actor.getId());
        message.setFolder(folder);
    }

    public void broadcast(BroadcastForm broadcastForm){
        Message message = broadCastreconstruct(broadcastForm);
        Assert.notNull(message);
        Administrator administrator = (Administrator) actorService.getPrincipal();
        String subject = message.getSubject();
        String body = message.getBody();
        String priority = message.getPriority();
        Collection<Actor> actors = actorService.findAll();
        actors.remove(administrator);
        for (Actor a : actors){
            Message m =this.create();
            m.setSubject(subject);
            m.setBody(body);
            m.setPriority(priority);
            m.setRecipient(a);
            this.saveToSend(m,true);
        }
    }


    public void delete(final Integer messageId) {
        Assert.isTrue(messageId != 0);
        Message message;
        message = this.messageRepository.findOne(messageId);
        this.checkPrincipalActor(message);
        final String folderName = message.getFolder().getName();
        if (folderName.equals("trash box") && message.getFolder().getSystem())
            this.delete(message);
        else {
            final Folder folder = this.folderService.findSystemFolder("trash box");
            message.setFolder(folder);
            this.save(message);
        }
    }



    public void move(final Integer messageId, final Integer folderId) {
        Folder folder;
        Message message;

        folder = this.folderService.findOne(folderId);
        this.folderService.checkPrincipalActor(folder);
        message = this.findOne(messageId);

        message.setFolder(folder);

        this.save(message);
    }











    public void spam(final Message message) {
        final Actor a = this.actorService.getPrincipal();
        final Folder f = this.folderService.getFoldersByActor(a.getId()).get(4);
        this.messageRepository.findOne(message.getId()).setFolder(f);
        this.messageRepository.save(message);
        f.getMessages().add(message);

        this.folderService.save(f);

    }

    public Message save(final Message message) {
        Message result;
        Assert.notNull(message);

        //CONCURRENCY CHECK
        if (message.getId() != 0) {
            final Message messageCheck = this.messageRepository.findOne(message.getId());
            Assert.isTrue(message.getVersion() == messageCheck.getVersion());
        }
        result = this.messageRepository.save(message);

        return result;
    }



    public void delete(final Message message) {
        Assert.notNull(message);

        this.messageRepository.delete(message);
    }



    //Other Business Methods -----------------------------------------------------------

    public Message findOneDisplay(final int messageId) {
        Message result;

        result = this.findOne(messageId);
        this.checkPrincipalActor(result);

        return result;
    }



    public Collection<Message> findMessagesByFolder(final int folderId) {
        Assert.notNull(folderId);
        Folder folder;

        folder = this.folderService.findOne(folderId);
        this.checkPrincipalFolderActor(folder);

        Collection<Message> result;

        result = this.messageRepository.findMessagesByFolderId(folderId);
        Assert.notNull(result);

        return result;
    }



    public void checkPrincipalActor(final Message message) {
        Assert.notNull(message);

        Actor actor;

        actor = this.actorService.getPrincipal();

        Assert.isTrue(actor.getId() == message.getFolder().getActor().getId());

    }

    private void checkPrincipalFolderActor(final Folder folder) {
        Assert.notNull(folder);

        Actor actor;

        actor = this.actorService.getPrincipal();

        Assert.isTrue(actor.getId() == folder.getActor().getId());

    }

    public Message moveMessage(final MessageMoveForm messageMoveForm) {
        Message mes = messageMoveForm.getMessage();
        final Folder fol = messageMoveForm.getFolder();
        mes.setFolder(fol);
        mes = this.save(mes);
        return mes;
    }


    public Message reconstruct(MessageForm messageForm){
        Message result;

        result = this.create();

        result.setSubject(messageForm.getSubject());
        result.setBody(messageForm.getBody());
        result.setPriority(messageForm.getPriority());
        result.setRecipient(actorService.findOne(messageForm.getActorId()));



        return result;
    }

    public Message broadCastreconstruct(BroadcastForm messageForm){
        Message result;

        result = this.create();

        result.setSubject(messageForm.getSubject());
        result.setBody(messageForm.getBody());
        result.setPriority(messageForm.getPriority());
        return result;
    }

}
