package repositories;
import java.util.Collection;
import java.util.Date;

import domain.Volume;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Article;
import domain.Newspaper;
@Repository
public interface NewspaperRepository extends JpaRepository<Newspaper, Integer> {

	@Query("select n from Newspaper n where n.draft = false")
	Collection<Newspaper> findAllNoDraft();
	
	@Query("select n from Newspaper n where n.draft = false and (n.title like %?1% or n.description like %?1%) ")
	Collection<Newspaper> findByKeyword(String keyword);
	
	@Query("select n from Newspaper n where n.user.id = ?1")
	Collection<Newspaper> findAllByUser(Integer id);

	@Query("select n from Newspaper n join n.articles a where n.user.id = ?1 and n.draft=true and n.articles.size > 0 and a.draft = false")
	Collection<Newspaper> findToPublish(Integer userId);

	@Query("select n from Newspaper n where n.user.id = ?1 and n.draft=true ")
	Collection<Newspaper> findByUserAndDraft(Integer userId);
	
	@Query("select distinct a from TabooWord tw, Newspaper a where a.title like CONCAT(CONCAT('%',tw.word),'%') or a.description like CONCAT(CONCAT('%',tw.word),'%')")
    Collection<Newspaper> containsTabooWord();
	
	@Query("select n from Newspaper n where n.draft = false and n.advertisements.size = 0")
	Collection<Newspaper> findAllWithNoAdd();

	@Query("select n from Newspaper n where n.draft = false and not n.advertisements.size = 0")
	Collection<Newspaper> findAllWithAdd();
}
