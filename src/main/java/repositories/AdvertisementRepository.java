package repositories;
import java.util.Collection;
import java.util.Date;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import domain.Advertisement;
@Repository
public interface AdvertisementRepository extends JpaRepository<Advertisement, Integer> {

	@Query("select distinct a from TabooWord tw, Advertisement a where a.title like CONCAT(CONCAT('%',tw.word),'%')")
    Collection<Advertisement> containsTabooWord();

}
