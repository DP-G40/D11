package repositories;
import java.util.Collection;
import java.util.Date;

import domain.Newspaper;
import domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import domain.Chirp;
@Repository
public interface ChirpRepository extends JpaRepository<Chirp, Integer> {

    @Query("select n from Chirp n where n.user.id = ?1")
    Collection<Chirp> findAllByUser(Integer id);
	@Query("select distinct a from TabooWord tw, Chirp a where a.title like CONCAT(CONCAT('%',tw.word),'%') or a.description like CONCAT(CONCAT('%',tw.word),'%')")
    Collection<Chirp> containsTabooWord();

    @Query("Select c from Chirp c where c.user in ?1  order by c.publicationDate desc")
    Collection<Chirp> findChirpByFollowing(Collection<User> users);

}
