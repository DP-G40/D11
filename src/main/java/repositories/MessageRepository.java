package repositories;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import domain.Actor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import domain.Message;
@Repository
public interface MessageRepository extends JpaRepository<Message, Integer> {


    //Messages by folder
    @Query("select m from Message m where m.folder.id=?1 order by m.priority desc")
    Collection<Message> findMessagesByFolderId(int folderId);

    @Query("select message from Message message where message.moment = ?1 and message.sender = ?2")
    List<Message> findMessageBySendMomentAndSender(Date date, Actor actor);

}
