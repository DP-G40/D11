package repositories;
import java.util.Collection;
import java.util.Date;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import domain.Article;
import domain.Newspaper;
@Repository
public interface ArticleRepository extends JpaRepository<Article, Integer> {

	@Query("select a from Article a where a.draft = false and a.newspaper.id = ?1")
	Collection<Article> findAllNoDraft(Integer newspaperId);
	
	@Query("select a from Article a where a.draft = false")
	Collection<Article> findAllNoDraftWithourNewspaper();
	
	@Query("select a from Article a where a.draft = false and (a.title like %?1% or a.summary like %?1% or a.body like %?1%) ")
	Collection<Article> findByKeyword(String keyword);
	
	@Query("select distinct a from TabooWord tw, Article a where a.title like CONCAT(CONCAT('%',tw.word),'%') or a.summary like CONCAT(CONCAT('%',tw.word),'%') or a.body like CONCAT(CONCAT('%',tw.word),'%')")
    Collection<Article> containsTabooWord();
}
