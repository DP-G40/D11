package repositories;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import domain.Administrator;
import domain.Newspaper;
@Repository
public interface AdministratorRepository extends JpaRepository<Administrator, Integer> {

	//C
	@Query("select avg(u.newspapers.size),stddev(u.newspapers.size) from User u")
	List<Object> Q1();
	
	@Query("select avg(u.articles.size),stddev(u.articles.size) from User u")
	List<Object> Q2();
	
	@Query("select avg(n.articles.size),stddev(n.articles.size) from Newspaper n")
	List<Object> Q3();
	
	@Query("select n from Newspaper n where n.articles.size>=(select avg(nn.articles.size) from Newspaper nn)*1.10")
	List<Newspaper> Q4();
	
	@Query("select n from Newspaper n where n.articles.size>=(select avg(nn.articles.size) from Newspaper nn)*0.90")
	List<Newspaper> Q5();
	
	@Query("select sum(case when u.newspapers.size>0 then 1.00 else 0.00 end)/count(u)*1.0 from User u")
	Double Q6();
	
	@Query("select sum(case when u.articles.size>0 then 1.00 else 0.00 end)/count(u)*1.0 from User u")
	Double Q7();
	
	//B
	@Query("select avg(a.followUps.size) from Article a")
	Double Q8();
	
	@Query("select count(fu)*1.0/(select count(a2)*1.0 from Article a2) from Article a join a.followUps fu join a.newspaper n where (n.publicationDate + 7) > fu.publicationDate")
	Double Q9(); 
	
	@Query("select count(fu)*1.0/(select count(a2)*1.0 from Article a2) from Article a join a.followUps fu join a.newspaper n where (n.publicationDate + 14) > fu.publicationDate")
	Double Q10(); 
	
	@Query("select avg(u.chirps.size),stddev(u.chirps.size) from User u")
	List<Object> Q11();
	
	@Query("select count(u)*1.0/(select count(u2)*1.0 from User u2) from User u where u.chirps.size>=(select avg(uu.chirps.size) from User uu)*0.75")
	Double Q12();
	
	//A
	
	@Query("select COALESCE(count(n)*1.0/(select count(n2)*1.0 from Newspaper n2 where n2.privateFlag = false),0.0) from Newspaper n where n.privateFlag = true")
	Double Q13();
	
	@Query("select COALESCE((select count(a)*1.0 from Article a)/count(n)*1.0,0.0) from Newspaper n where n.privateFlag = true")
	Double Q14();
	
	@Query("select COALESCE((select count(a)*1.0 from Article a)/count(n)*1.0,0.0) from Newspaper n where n.privateFlag = false")
	Double Q15();
	
	@Query("select COALESCE(sum(n.subscriptions.size)*1.0/(select count(c)*1.0 from Customer c),0.0) from Newspaper n where n.privateFlag = true")
	Double Q16();
	
	@Query("select COALESCE((sum(case when n.privateFlag = true then 1.00 else 0.00 end)/sum(case when n.privateFlag = false then 1.00 else 0.00 end))/count(u)*1.0,0.0) from User u join u.newspapers n")
	Double Q17();
	
	//2C
	
	@Query("select sum(case when n.advertisements.size > 0 then 1.00 else 0.00 end)*1.0/sum(case when n.advertisements.size = 0 then 1.00 else 0.00 end)*1.0 from Newspaper n")
	Double Q18();
	
	@Query("select COALESCE(count(distinct a)*1.0/count(distinct a2)*1.0, 0.0) from TabooWord tw, Advertisement a, Advertisement a2 where a.title like CONCAT(CONCAT('%',tw.word),'%')")
    Double Q19();
	
	//2B
	@Query("select COALESCE(avg(v.newspapers.size), 0.0) from Volume v")
	Double Q20();
	
	@Query("select COALESCE((sum(case when s.volume is not null then 1.00 else 0.00 end)*1.0)/(sum(case when s.newspaper is not null then 1.00 else 0.00 end)*1.0), 0.0) from Subscription s")
	Double Q21();

}
