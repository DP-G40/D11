package controllers;

import domain.Article;
import domain.Chirp;
import domain.User;
import forms.RegisterForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.ActorService;
import services.ChirpService;
import services.UserService;

import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("/user/profiles")
public class UserController extends AbstractController {

	@Autowired
	private UserService userService;

	@Autowired
	private ActorService actorService;

	@Autowired
	private ChirpService chirpService;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelAndView list() {
		ModelAndView result;
		Collection<User> users = userService.findAll();
		result = new ModelAndView("user/list");
		result.addObject("user", users);
		result.addObject("requestURI", "/user/list.do");
		return result;
	}

	@RequestMapping(value = "/followers", method = RequestMethod.GET)
	public ModelAndView followers() {
		ModelAndView result;
		Integer idUser = actorService.findByPrincipal().getId();
		Collection<User> users = userService.findOne(idUser).getFollowers();
		result = new ModelAndView("user/followers");
		result.addObject("user", users);
		result.addObject("requestURI", "/user/list.do");
		return result;
	}

	@RequestMapping(value = "/following", method = RequestMethod.GET)
	public ModelAndView following() {
		ModelAndView result;
		Integer idUser = actorService.findByPrincipal().getId();
		Collection<User> users = userService.findOne(idUser).getFollowing();
		result = new ModelAndView("user/following");
		result.addObject("user", users);
		result.addObject("requestURI", "/user/list.do");
		return result;
	}


	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public ModelAndView view(@RequestParam Integer UserId) {
		ModelAndView result;
		try {
			result = new ModelAndView("user/view");
			User user = userService.findOne(UserId);
			if(user==null) {
				user = (User) actorService.findByUserAccount(UserId);
			}
			Collection<Article> articles = user.getArticles();

			result.addObject("user", user);
			result.addObject("articles", articles);


			try{
				User usuarioconectado = (User) actorService.findByPrincipal();
				if(usuarioconectado != user){
					if(usuarioconectado.getFollowing().contains(user)){
						//Lo sigo
						result.addObject("follow", true);
					}else {
						//No lo sigo
						result.addObject("follow", false);
					}
				}

			}catch (Throwable oops){

			}
			try{
				Collection<Chirp> chirps = chirpService.findAllByUser(user.getId());
				result.addObject("chirps",chirps);

			}catch (Throwable oops){

			}
		} catch (Throwable oops){
			result = new ModelAndView("redirect:/#");
		}
		return result;
	}

	@RequestMapping(value = "/follow", method = RequestMethod.GET)
	public ModelAndView follow(@RequestParam Integer userId) {
		ModelAndView result;
		try{
			result = new ModelAndView("user/view");
			User user = userService.findOne(userId);
			if(user==null) {
				user = (User) actorService.findByUserAccount(userId);
			}
			User usuarioconectado = (User) actorService.findByPrincipal();
			usuarioconectado.getFollowing().add(user);
			userService.save(usuarioconectado);
			Boolean flag = userService.followable(usuarioconectado,user);
			result.addObject("follow", flag);
			result.addObject("user", user);
			result.addObject("articles", user.getArticles());
			result.addObject("chirps",chirpService.findAllByUser(user.getId()));
		} catch (Throwable oops){
			result = new ModelAndView("redirect:/#");
		}

		return result;
	}

	@RequestMapping(value = "/unfollow", method = RequestMethod.GET)
	public ModelAndView unfollow(@RequestParam Integer userId) {
		ModelAndView result;
		try{
			result = new ModelAndView("user/view");
			User user = userService.findOne(userId);
			if(user==null) {
				user = (User) actorService.findByUserAccount(userId);
			}
			User usuarioconectado = (User) actorService.findByPrincipal();
			usuarioconectado.getFollowing().remove(user);
			userService.save(usuarioconectado);
			Boolean flag = userService.followable(usuarioconectado,user);
			result.addObject("follow", flag);
			result.addObject("user", user);
			result.addObject("articles", user.getArticles());
			result.addObject("chirps",chirpService.findAllByUser(user.getId()));
		} catch (Throwable oops){
			result = new ModelAndView("redirect:/#");
		}

		return result;
	}


}
