/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.util.Collection;

import domain.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import repositories.NewspaperRepository;
import services.*;

import domain.Article;
import domain.Newspaper;

@Controller
@RequestMapping("/article/actor")
public class ArticleController extends AbstractController {

	// Constructors -----------------------------------------------------------

	public ArticleController() {
		super();
	}
	
	@Autowired
    private ArticleService articleService;
	
	@Autowired
    private ActorService actorService;

	@Autowired
	private AdministratorService administratorService;

	@Autowired
	private CustomerService customerService;

	@Autowired
	private SubscriptionService subscriptionService;

	@Autowired
	private NewspaperController nc;
	// Action-1 ---------------------------------------------------------------		

	@RequestMapping(value="/list", method = RequestMethod.GET)
	public ModelAndView action1(@RequestParam final int newspaperId) {
		ModelAndView result;

		Collection<Article> articles = articleService.findByNewspaper(newspaperId);
		result = new ModelAndView("article/list");
		result.addObject("articles", articles);
		result.addObject("requestURI", "/article/actor/list.do");
		
		return result;
	}

	@RequestMapping("/view")
	public ModelAndView view(@RequestParam final int articleId) {
		ModelAndView result;

		try {
			Article article = articleService.findOne(articleId);
			if(article.getNewspaper().getPrivateFlag()==true){
				//Si estas logueado
				try{
					Integer iduserconectado = actorService.findByPrincipal().getId();
					if(iduserconectado == article.getUser().getId()){
						//Si eres el creador
						result = new ModelAndView("article/view");
						result.addObject("banner", articleService.getRandomAnnouncement(article));
						result.addObject("article", article);
						result.addObject("requestURI", "/article/actor/view.do");
					}else if(administratorService.findOne(iduserconectado) != null){
						//Si eres el administrador
						result = new ModelAndView("article/view");
						result.addObject("article", article);
						result.addObject("banner", articleService.getRandomAnnouncement(article));
						result.addObject("requestURI", "/article/actor/view.do");
					}else if(customerService.findOne(iduserconectado) != null){
						//Si eres un customer
						Integer subOrNot = subscriptionService.findAllByCustomerAndNewspaper(article.getNewspaper().getId());
						if(subOrNot>0){
							//Si est�s suscrito
							result = new ModelAndView("article/view");
							result.addObject("article", article);
							result.addObject("banner", articleService.getRandomAnnouncement(article));
							result.addObject("requestURI", "/article/actor/view.do");
						}else{
							//Si no est�s suscrito
							result = nc.view(article.getNewspaper().getId());
						}

					}else {
						//Cualquier otra cosa
						result = nc.view(article.getNewspaper().getId());
					}
				}catch (Throwable oops){
					//Si no estas logueado
					result = nc.view(article.getNewspaper().getId());
				}


			}else {
				result = new ModelAndView("article/view");
				result.addObject("article", article);
				result.addObject("banner", articleService.getRandomAnnouncement(article));
				result.addObject("requestURI", "/article/actor/view.do");
				try{
					result.addObject("actorId", actorService.findByPrincipal().getId());
				}catch(Exception e){
					result.addObject("actorId", 0);
				}
			}




		}catch (Throwable oops){
			result = new ModelAndView("redirect:/#");
		}

		return result;

	}
	
	@RequestMapping(value="/search", method = RequestMethod.GET)
	public ModelAndView action2(@RequestParam final String keyword) {
		ModelAndView result;

		Collection<Article> articles = articleService.findByKeyword(keyword);
		result = new ModelAndView("article/list");
		result.addObject("articles", articles);
		result.addObject("requestURI", "/article/actor/list.do");
		
		return result;
	}
	
	@RequestMapping(value="/listAll")
	public ModelAndView action3() {
		ModelAndView result;

		Collection<Article> articles = articleService.findAllNoDraftWithourNewspaper();
		result = new ModelAndView("article/list");
		result.addObject("articles", articles);
		result.addObject("requestURI", "/article/actor/list.do");
		
		return result;
	}

}
