/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import domain.Actor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import services.AdministratorService;

@Controller
@RequestMapping("/administrator")
public class AdministratorController extends AbstractController {

	// Constructors -----------------------------------------------------------

	public AdministratorController() {
		super();
	}
	
	//SERVICIOS
	
	@Autowired
	private AdministratorService	administratorService;

	//DASHBOARD
	
	@RequestMapping("/dashboard")
	public ModelAndView dashboard(){
		
		ModelAndView result;
		result = new ModelAndView("administrator/dashboard");
		
		result.addObject("Q1", administratorService.Q1());
		result.addObject("Q2", administratorService.Q2());
		result.addObject("Q3", administratorService.Q3());
		result.addObject("Q4", administratorService.Q4());
		result.addObject("Q5", administratorService.Q5());
		result.addObject("Q6", administratorService.Q6());
		result.addObject("Q7", administratorService.Q7());
		result.addObject("Q8", administratorService.Q8());
		result.addObject("Q9", administratorService.Q9());
		result.addObject("Q10", administratorService.Q10());
		result.addObject("Q11", administratorService.Q11());
		result.addObject("Q12", administratorService.Q12());
		result.addObject("Q13", administratorService.Q13());
		result.addObject("Q14", administratorService.Q14());
		result.addObject("Q15", administratorService.Q15());
		result.addObject("Q16", administratorService.Q16());
		result.addObject("Q17", administratorService.Q17());
		result.addObject("Q18", administratorService.Q18());
		result.addObject("Q19", administratorService.Q19());
		result.addObject("Q20", administratorService.Q20());
		result.addObject("Q21", administratorService.Q21());
		
		return result;
	}

}
