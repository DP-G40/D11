/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers;

import java.util.Collection;

import domain.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import repositories.NewspaperRepository;
import services.ActorService;
import services.ArticleService;
import services.NewspaperService;
import services.SubscriptionService;

import domain.Newspaper;

@Controller
@RequestMapping("/newspaper/actor")
public class NewspaperController extends AbstractController {

	// Constructors -----------------------------------------------------------

	public NewspaperController() {
		super();
	}
	
	@Autowired
    private NewspaperService newspaperService;

	@Autowired
	private ArticleService articleService;
	
	@Autowired
    private ActorService actorService;
	
	@Autowired
    private SubscriptionService subscriptionService;

	// Action-1 ---------------------------------------------------------------		

	@RequestMapping("/list")
	public ModelAndView action1() {
		ModelAndView result;

		Collection<Newspaper> newspapers = newspaperService.findAllNoDraft();
		result = new ModelAndView("newspaper/list");
		result.addObject("newspapers", newspapers);
		result.addObject("requestURI", "/newspaper/actor/list.do");
		
		return result;
	}
	
	


	@RequestMapping("/view")
	public ModelAndView view(@RequestParam final int newspaperId) {
		ModelAndView result;

		try {
			Newspaper newspaper = newspaperService.findOne(newspaperId);
			Assert.isTrue(newspaper.getDraft() == false || newspaper.getUser().getId() == actorService.findByPrincipal().getId());
			Collection<Article> articles=articleService.findByNewspaper(newspaperId);
			result = new ModelAndView("newspaper/view");
			result.addObject("newspaper", newspaper);
			result.addObject("articles", articles);
			result.addObject("requestURI", "/newspaper/actor/view.do");

			//Miramos si hay login echo y si es un customer, mirar� si ya est� subscrito.
			try{
				Integer subOrNot = subscriptionService.findAllByCustomerAndNewspaper(newspaperId);
				if(subOrNot>0){
					result.addObject("mostrarBotonSub", false);
				}else{
					result.addObject("mostrarBotonSub", true);
				}
				result.addObject("actorId", actorService.findByPrincipal().getId());
			}catch(Exception e){
				result.addObject("mostrarBotonSub", false);
			}

		}catch (Throwable oops){
			result = new ModelAndView("redirect:/#");
		}

		return result;

	}

	@RequestMapping(value="/search", method = RequestMethod.GET)
	public ModelAndView action2(@RequestParam final String keyword) {
		ModelAndView result;

		Collection<Newspaper> newspapers = newspaperService.findByKeyword(keyword);
		result = new ModelAndView("newspaper/list");
		result.addObject("newspapers", newspapers);
		result.addObject("requestURI", "/newspaper/actor/list.do");
		
		return result;
	}
	
}
