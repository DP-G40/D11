package controllers.unauthenticate;

import controllers.AbstractController;
import domain.Agent;
import forms.RegisterForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import services.AgentService;

import javax.validation.Valid;

@Controller
@RequestMapping("/agent")
public class AgentUnautheticatedController extends AbstractController{

    @Autowired
    private AgentService agentService;

    @RequestMapping(value = "/register",method = RequestMethod.GET)
    public ModelAndView register(){
        ModelAndView res = new ModelAndView("agent/register");
        try {
            RegisterForm registerForm = new RegisterForm();
            res.addObject("registerForm",registerForm);
        } catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }


    @RequestMapping(value = "register",method = RequestMethod.POST,params = "save")
    public ModelAndView save(@Valid RegisterForm registerForm, BindingResult result){
        ModelAndView res = new ModelAndView("agent/register");
        res.addObject("registeForm",registerForm);
        if (!result.hasErrors()){
            try {
                Agent agent = agentService.reconstruct(registerForm);
                agentService.save(agent);
                res = new ModelAndView("redirect:/security/login.do");
            } catch (Throwable oops){
                if (oops.getMessage().equals("register.error.password")) {
                    res.addObject("message", "register.error.password");
                } else if (oops.getMessage().equals("register.error.username")) {
                    res.addObject("message", "register.error.username");
                }
            }
        }
        return res;
    }
}
