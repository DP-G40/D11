package controllers.unauthenticate;

import controllers.AbstractController;
import domain.Customer;
import domain.User;
import forms.RegisterForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import services.CustomerService;
import services.UserService;

import javax.validation.Valid;

@Controller
@RequestMapping("/customer")
public class CustomerUnauthenticateController extends AbstractController {

	@Autowired
	private CustomerService customerService;

	@RequestMapping(value = "register")
	public ModelAndView register() {
		ModelAndView res = new ModelAndView("customer/register");
		RegisterForm registerForm = new RegisterForm();
		res.addObject("registerForm", registerForm);
		return res;

	}

	@RequestMapping(value = "register", method = RequestMethod.POST, params = "save")
	public ModelAndView save(@Valid RegisterForm registerForm,
			BindingResult result) {
		ModelAndView res = new ModelAndView("customer/register");
		res.addObject("registerForm", registerForm);
		if (!result.hasErrors()) {
			try {
				Customer customer = customerService.reconstruct(registerForm);
				customerService.save(customer);
				res = new ModelAndView("redirect:/security/login.do");
			} catch (Throwable oops) {
				if (oops.getMessage().equals("register.error.password")) {
					res.addObject("message", "register.error.password");
				} else if (oops.getMessage().equals("register.error.username")) {
					res.addObject("message", "register.error.username");
				}
			}

		}
		return res;
	}
}
