package controllers.user;

import controllers.AbstractController;
import domain.Volume;
import forms.AddNewspaperForm;
import forms.VolumeForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.NewspaperService;
import services.VolumeService;

import javax.validation.Valid;

@Controller
@RequestMapping("/volume/user")
public class VolumeUserController extends AbstractController {


    @Autowired
    private VolumeService volumeService;

    @Autowired
    private NewspaperService newspaperService;

    @RequestMapping(value = "/list",method = RequestMethod.GET)
    public ModelAndView list(){
        ModelAndView res = new ModelAndView("volume/list");
        try{
            res.addObject("volumes",volumeService.findByUser());
            res.addObject("requestUri","/volume/user/list.do");
            res.addObject("mine",true);
        } catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }

    @RequestMapping(value = "/create",method = RequestMethod.GET)
    public ModelAndView create(){
        ModelAndView res = new ModelAndView("volume/create");
        VolumeForm volumeForm = new VolumeForm();
        res.addObject("volumeForm",volumeForm);
        res.addObject("action","volume/user/create.do");
        return res;
    }

    @RequestMapping(value = "/create",method = RequestMethod.POST,params = "save")
    public ModelAndView saveCreate(@Valid VolumeForm volumeForm, BindingResult result){
        ModelAndView res = new ModelAndView("volume/create");
        res.addObject("volumeForm",volumeForm);
        res.addObject("action","volume/user/create.do");
        if (!result.hasErrors()){
            try {
                Volume volume = volumeService.reconstruct(volumeForm);
                volume = volumeService.save(volume);
                res = new ModelAndView("redirect:/volume/user/list.do");
            } catch (Throwable oops){

            }
        }
        return res;
    }

    @RequestMapping(value = "/edit",method = RequestMethod.GET)
    public ModelAndView edit(@RequestParam Integer volumeId){
        ModelAndView res = new ModelAndView("volume/edit");
        try{
            volumeService.isMine(volumeId);
            VolumeForm volumeForm = volumeService.construct(volumeId);
            volumeForm.setVolumeId(volumeId);
            res.addObject("volumeForm",volumeForm);
            res.addObject("action","volume/user/edit.do");
        } catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }

    @RequestMapping(value = "/edit",method = RequestMethod.POST,params = "save")
    public ModelAndView saveEdit(@Valid VolumeForm volumeForm, BindingResult result){
        ModelAndView res = new ModelAndView("volume/edit");
        res.addObject("action","volume/user/edit.do");
        res.addObject("volumeForm",volumeForm);
        if (!result.hasErrors()){
            try {
                Volume volume = volumeService.reconstruct(volumeForm);
                volume = volumeService.save(volume);
                res = new ModelAndView("redirect:/volume/user/list.do");
            } catch (Throwable oops){

            }
        }
        return res;
    }

    @RequestMapping(value = "/add",method = RequestMethod.GET)
    public ModelAndView addNewspaper(@RequestParam Integer volumeId){
        ModelAndView res = new ModelAndView("volume/add");
        try {
            AddNewspaperForm addNewspaperForm = new AddNewspaperForm();
            volumeService.isMine(volumeId);
            addNewspaperForm.setVolumeId(volumeId);
            res.addObject("addNewspaperForm",addNewspaperForm);
            res.addObject("newspapers",newspaperService.findNotInVolume(volumeId));
        } catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }

    @RequestMapping(value = "/add",method = RequestMethod.POST,params = "save")
    public ModelAndView saveAddNewspaper(@Valid AddNewspaperForm addNewspaperForm, BindingResult result){
        ModelAndView res = new ModelAndView("volume/add");
        res.addObject("addNewspaperForm",addNewspaperForm);
        res.addObject("newspapers",newspaperService.findNotInVolume(addNewspaperForm.getVolumeId()));
        if (!result.hasErrors()) {
            try {
                volumeService.addNewspaper(addNewspaperForm);
                res = new ModelAndView("redirect:/volume/view.do?volumeId="+addNewspaperForm.getVolumeId());
            } catch (Throwable oops){

            }
        }
        return res;
    }
    @RequestMapping(value = "/quit",method = RequestMethod.GET)
    public ModelAndView quit(@RequestParam Integer newspaperId,@RequestParam Integer volumeId){
        ModelAndView res = new ModelAndView("redirect:/volume/view.do?volumeId="+volumeId);
        try {
            volumeService.quitNewspaper(volumeId,newspaperId);
        } catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }




}
