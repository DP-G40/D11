package controllers.user;

import controllers.AbstractController;
import domain.Article;
import domain.User;
import forms.ArticleForm;
import forms.PictureForm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.ActorService;
import services.ArticleService;
import services.NewspaperService;
import services.PictureService;

import javax.validation.Valid;
import java.util.Date;

@Controller
@RequestMapping("/article/user")
public class ArticleUserController extends AbstractController{

    public ArticleUserController(){
        super();
    }

    @Autowired
    private ArticleService articleService;

    @Autowired
    private NewspaperService newspaperService;

    @Autowired
    private ActorService actorService;

     @Autowired
     private PictureService pictureService;

    @RequestMapping(value = "/create",method = RequestMethod.GET)
    public ModelAndView create(){
        ModelAndView res = new ModelAndView("article/create");
        ArticleForm articleForm = new ArticleForm();
        res.addObject("articleForm",articleForm);
        res.addObject("newspaper",newspaperService.findByUserAndDraft());
        res.addObject("action","article/user/create.do");
        return res;
    }

    @RequestMapping(value = "/create",method = RequestMethod.POST,params = "save")
    public ModelAndView saveCreate(@Valid ArticleForm articleForm, BindingResult result){
        ModelAndView res = new ModelAndView("article/create");
        res.addObject("newspaper",newspaperService.findByUserAndDraft());
        res.addObject("action","article/user/create.do");
        if(!result.hasErrors()){
            try{
                Article article = articleService.reconstruct(articleForm);
                articleService.save(article);
                res = new ModelAndView("redirect:/article/user/list.do");
            } catch (Throwable oops){

            }
        }
        return res;
    }


    @RequestMapping(value = "/edit",method = RequestMethod.GET)
    public ModelAndView edit(@RequestParam Integer articleId){
        ModelAndView res = new ModelAndView("article/edit");
        try {
            articleService.isMine(articleId);
            Article article = articleService.findOne(articleId);
            Assert.isTrue(article.getDraft());
            ArticleForm articleForm = articleService.construct(article);
            res.addObject("articleForm", articleForm);
            res.addObject("newspaper", newspaperService.findByUserAndDraft());
            res.addObject("action", "article/user/edit.do");
        } catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }

    @RequestMapping(value = "/edit",method = RequestMethod.POST,params = "save")
    public ModelAndView saveEdit(@Valid ArticleForm articleForm, BindingResult result){
        ModelAndView res = new ModelAndView("article/edit");
        res.addObject("newspaper",newspaperService.findByUserAndDraft());
        res.addObject("action","article/user/edit.do");
        if(!result.hasErrors()){
            try{
                Article article = articleService.reconstruct(articleForm);
                articleService.isMine(article.getId());
                Assert.isTrue(article.getDraft());
                articleService.save(article);
                res = new ModelAndView("redirect:/article/user/list.do");
            } catch (Throwable oops){

            }
        }
        return res;
    }

    @RequestMapping(value = "/edit",method = RequestMethod.POST,params = "publish")
    public ModelAndView publish(@Valid ArticleForm articleForm, BindingResult result){
        ModelAndView res = new ModelAndView("article/edit");
        res.addObject("newspaper",newspaperService.findByUserAndDraft());
        res.addObject("action","article/user/edit.do");
        if(!result.hasErrors()){
            try{
                Article article = articleService.reconstruct(articleForm);
                articleService.isMine(article.getId());
                article.setDraft(false);
                article.setPublicationDate(new Date());
                articleService.save(article);
                res = new ModelAndView("redirect:/article/user/list.do");
            } catch (Throwable oops){

            }
        }
        return res;
    }

    @RequestMapping(value = "/edit",method = RequestMethod.POST,params = "delete")
    public ModelAndView delete(@Valid ArticleForm articleForm, BindingResult result){
        ModelAndView res = new ModelAndView("article/edit");
        res.addObject("newspaper",newspaperService.findByUserAndDraft());
        res.addObject("action","article/user/edit.do");
        if(!result.hasErrors()){
            try{
                Article article = articleService.reconstruct(articleForm);
                articleService.isMine(article.getId());
                Assert.isTrue(article.getDraft());
                articleService.delete(article);
                res = new ModelAndView("redirect:/article/user/list.do");
            } catch (Throwable oops){

            }
        }
        return res;
    }

    @RequestMapping(value = "/list",method = RequestMethod.GET)
    public ModelAndView list(){
        ModelAndView res = new ModelAndView("article/list");
        try{
            User user = (User) actorService.findByPrincipal();
            res.addObject("articles",user.getArticles());
            res.addObject("mine",true);
        } catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }

    @RequestMapping(value = "/picture",method = RequestMethod.GET)
    public ModelAndView addPicture(@RequestParam Integer articleId){
        ModelAndView res = new ModelAndView("article/picture");
        try{
            articleService.isMine(articleId);
            PictureForm pictureForm = new PictureForm();
            pictureForm.setArticleId(articleId);
            res.addObject("pictureForm",pictureForm);
            res.addObject("action","article/user/picture.do");
        } catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }

    @RequestMapping(value = "/picture",method = RequestMethod.POST, params = "save")
    public ModelAndView addPictureSave(@Valid PictureForm pictureForm, BindingResult result){
        ModelAndView res = new ModelAndView("article/picture");
        res.addObject("pictureForm",pictureForm);
        res.addObject("action","article/user/picture.do");
        if(!result.hasErrors()){
            try{
                pictureService.reconstruct(pictureForm);
                res = new ModelAndView("redirect:/article/user/list.do");
            } catch (Throwable oops){
                System.out.println(oops);
            }
        }
        return res;
    }


}
