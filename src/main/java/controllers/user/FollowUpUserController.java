package controllers.user;

import controllers.AbstractController;
import domain.Article;
import domain.FollowUp;
import domain.Picture;
import domain.User;
import forms.ArticleForm;
import forms.PictureForm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.ActorService;
import services.ArticleService;
import services.FollowUpService;
import services.NewspaperService;
import services.PictureService;
import services.UserService;

import javax.validation.Valid;
import java.util.Date;

@Controller
@RequestMapping("/followUp/user")
public class FollowUpUserController extends AbstractController{

    public FollowUpUserController(){
        super();
    }

    @Autowired
    private ArticleService articleService;

    @Autowired
    private FollowUpService followUpService;

    @Autowired
    private ActorService actorService;

    @Autowired
    private UserService userService;

    @Autowired
    private PictureService pictureService;

    @RequestMapping(value = "/create",method = RequestMethod.GET)
    public ModelAndView create(@RequestParam Integer articleId){
        ModelAndView res = new ModelAndView("followUp/create");
        Article article = articleService.findOne(articleId);
        FollowUp followUp = followUpService.create();
        followUp.setArticle(article);
        followUp.setPublicationDate(new Date());
        followUp.setUser(userService.findOne(actorService.getPrincipal().getId()));
        res.addObject("followUp",followUp);
        return res;
    }

    @RequestMapping(value = "/create",method = RequestMethod.POST,params = "save")
    public ModelAndView saveCreate(@Valid FollowUp followUp, BindingResult result){
        ModelAndView res = new ModelAndView("followUp/create");
        res.addObject("followUp",followUp);
        if(!result.hasErrors()){
            try{
            	followUpService.save(followUp);
                res = new ModelAndView("redirect:/article/actor/view.do?articleId="+followUp.getArticle().getId());
            } catch (Throwable oops){
            	 res = new ModelAndView("redirect:#");
            }
        }
        return res;
    }

    @RequestMapping(value = "/picture",method = RequestMethod.GET)
    public ModelAndView addPicture(@RequestParam Integer followUpId){
        ModelAndView res = new ModelAndView("article/picture");
        try{
            followUpService.isMine(followUpId);
            PictureForm pictureForm = new PictureForm();
            pictureForm.setArticleId(followUpId);
            res.addObject("pictureForm",pictureForm);
            res.addObject("action","followUp/user/picture.do");
        } catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }

    @RequestMapping(value = "/picture",method = RequestMethod.POST, params = "save")
    public ModelAndView addPictureSave(@Valid PictureForm pictureForm, BindingResult result){
        ModelAndView res = new ModelAndView("article/picture");
        res.addObject("action","followUp/user/picture.do");
        res.addObject("pictureForm",pictureForm);
        if(!result.hasErrors()){
            try{
                pictureService.reconstruct(pictureForm,true);
                res = new ModelAndView("redirect:/article/user/list.do");
            } catch (Throwable oops){
                System.out.println(oops);
            }
        }
        return res;
    }

}
