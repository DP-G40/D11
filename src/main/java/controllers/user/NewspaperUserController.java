/*
 * AdministratorController.java
 * 
 * Copyright (C) 2017 Universidad de Sevilla
 * 
 * The use of this project is hereby constrained to the conditions of the
 * TDG Licence, a copy of which you may download from
 * http://www.tdg-seville.info/License.html
 */

package controllers.user;

import java.util.Collection;

import controllers.AbstractController;
import forms.NewspaperForm;
import forms.PublishForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.ActorService;
import services.NewspaperService;

import domain.Newspaper;
import services.PictureService;

import javax.validation.Valid;

@Controller
@RequestMapping("/newspaper/user")
public class NewspaperUserController extends AbstractController {

	// Constructors -----------------------------------------------------------

	public NewspaperUserController() {
		super();
	}
	
	@Autowired
    private NewspaperService newspaperService;
	
	@Autowired
    private ActorService actorService;

	@Autowired
	private PictureService pictureService;

	// Action-1 ---------------------------------------------------------------		

	@RequestMapping(value="/publish", method=RequestMethod.GET)
	public ModelAndView publish() {
		ModelAndView res = new ModelAndView("newspaper/publish");
		PublishForm publishForm = new PublishForm();
		Collection<Newspaper> newspapers = newspaperService.findToPublish();
		res.addObject("publishForm",publishForm);
		res.addObject("newspapers",newspapers);
		if(newspapers.isEmpty()){
			res.addObject("disable",true);
			res.addObject("message","newspaper.noArticles");
		}else {
			res.addObject("disable",false);
		}
		return res;
	}


	@RequestMapping(value = "/publish",method = RequestMethod.POST,params = "save")
	public ModelAndView savePublish(@Valid PublishForm publishForm, BindingResult result){
		ModelAndView res = new ModelAndView("newspaper/publish");
		Collection<Newspaper> newspapers = newspaperService.findToPublish();
		res.addObject("newspapers",newspapers);
		if(newspapers.isEmpty()){
			res.addObject("disable",true);
			res.addObject("message","newspaper.noArticles");
		}else {
			res.addObject("disable",false);
		}
		if (!result.hasErrors()){
			try{
				newspaperService.publish(publishForm.getNewspaper(),publishForm.getPrivateFlag());
				res = new ModelAndView("redirect:/newspaper/user/list.do");
			}catch(Throwable oops){
				res.addObject("message","commit.error");
			}
		}
			return res;
	}

	@RequestMapping(value = "/create",method = RequestMethod.GET)
	public ModelAndView create(){
		ModelAndView res = new ModelAndView("newspaper/create");
		try {
			NewspaperForm newspaperForm = new NewspaperForm();
			res.addObject("newspaperForm", newspaperForm);
			res.addObject("action","newspaper/user/create.do");
		} catch (Throwable oops){
			res = new ModelAndView("redirect:/#");
		}
		return res;
	}

	@RequestMapping(value = "/create",method = RequestMethod.POST,params = "save")
	public ModelAndView save(@Valid NewspaperForm newspaperForm, BindingResult result){
		ModelAndView res = new ModelAndView("newspaper/create");
		res.addObject("newspaperForm", newspaperForm);
		res.addObject("action","newspaper/user/create.do");
		if (!result.hasErrors()){
			try{
				Newspaper newspaper = newspaperService.reconstruct(newspaperForm);
				newspaper = newspaperService.save(newspaper);
				pictureService.reconstruct(newspaper,newspaperForm);
				res = new ModelAndView("redirect:/newspaper/user/list.do");
			} catch (Throwable oops){
				System.out.println(oops);
			}
		}
		return res;
	}


	@RequestMapping(value = "/edit",method = RequestMethod.GET)
	public ModelAndView edit(@RequestParam Integer newspaperId){
		ModelAndView res = new ModelAndView("newspaper/edit");
		res.addObject("action","newspaper/user/edit.do");
		try {
			NewspaperForm newspaperForm = newspaperService.construct(newspaperId);
			res.addObject("newspaperForm", newspaperForm);
		} catch (Throwable oops){
			res = new ModelAndView("redirect:/#");
		}
		return res;
	}

	@RequestMapping(value = "/edit",method = RequestMethod.POST,params = "save")
	public ModelAndView saveEdit(@Valid NewspaperForm newspaperForm, BindingResult result){
		ModelAndView res = new ModelAndView("newspaper/edit");
		res.addObject("action","newspaper/user/edit.do");
		res.addObject("newspaperForm", newspaperForm);
		if (!result.hasErrors()){
			try{
				Newspaper newspaper = newspaperService.reconstruct(newspaperForm);
				newspaperService.isMine(newspaper,false);
				newspaper = newspaperService.save(newspaper);
				pictureService.reconstruct(newspaper,newspaperForm);
				res = new ModelAndView("redirect:/newspaper/user/list.do");
			} catch (Throwable oops){
				System.out.println(oops);
			}
		}
		return res;
	}




	@RequestMapping("/list")
	public ModelAndView list() {
		ModelAndView result;

		Integer idUser = actorService.findByPrincipal().getId();
		Collection<Newspaper> newspapers = newspaperService.findAllByUser(idUser);
		result = new ModelAndView("newspaper/list");
		result.addObject("newspapers", newspapers);
		result.addObject("idUser", idUser);
		result.addObject("requestURI", "/newspaper/user/list.do");
		result.addObject("mine", "true");

		return result;
	}
	
}
