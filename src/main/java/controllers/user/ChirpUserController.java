package controllers.user;

import controllers.AbstractController;

import domain.Chirp;
import domain.User;
import forms.ArticleForm;
import forms.ChirpForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import services.ActorService;
import services.ChirpService;
import services.PictureService;

import javax.validation.Valid;
import java.util.Collection;
import java.util.Date;

@Controller
@RequestMapping("/chirp/user")
public class ChirpUserController extends AbstractController {




    public ChirpUserController(){
        super();
    }

    @Autowired
    private ChirpService chirpService;

    @Autowired
    private ActorService actorService;

    @Autowired
    private PictureService pictureService;

    @RequestMapping(value="/create",method = RequestMethod.GET)
    public ModelAndView create() {
        ModelAndView result = new ModelAndView("chirp/create");
        try {
            ChirpForm chirpForm = new ChirpForm();
            result.addObject("chirpForm",chirpForm);
            result.addObject("action","chirp/user/create.do");
        }catch (Throwable oops){
            result = new ModelAndView("redirect:/#");
        }
        return result;
    }

    @RequestMapping(value="/create",method = RequestMethod.POST, params = "save")
    public ModelAndView saveCreate(@Valid ChirpForm chirpForm, BindingResult result) {
        ModelAndView res = new ModelAndView("chirp/create");
        User u = (User) actorService.getPrincipal();
        res.addObject("action","chirp/user/create.do");
        if(!result.hasErrors()){
            try {
                Chirp chirp = chirpService.reconstruct(chirpForm);
                chirp.setPublicationDate(new Date());
                chirp.setUser(u);
                chirpService.save(chirp);
                res = new ModelAndView("redirect:/user/profiles/view.do?UserId="+u.getId());

            }catch (Throwable oops){
                res = new ModelAndView("redirect:/#");
            }
        }

        return res;
    }

    @RequestMapping(value="/stream",method = RequestMethod.GET)
    public ModelAndView stream() {
        ModelAndView result = new ModelAndView("chirp/stream");
        User u = (User) actorService.getPrincipal();
        if(u.getFollowing().isEmpty()){
            result.addObject("cosa",true);

        }else {
            Collection<Chirp> chirps = chirpService.findChirpByFollowing(u.getFollowing());
            result.addObject("chirps",chirps);
            result.addObject("requestURI", "/chirps/user/list.do");
        }


        return result;
    }


}
