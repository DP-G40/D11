package controllers.agent;

import controllers.AbstractController;
import domain.Advertisement;
import forms.AdvertisementForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import services.AdvertisementService;
import services.NewspaperService;

import javax.validation.Valid;

@Controller
@RequestMapping("/advertisement/agent")
public class AdvertisementAgentController extends AbstractController {

    @Autowired
    private AdvertisementService advertisementService;

    @Autowired
    private NewspaperService newspaperService;

    @RequestMapping(value = "/create",method = RequestMethod.GET)
    public ModelAndView create(){
        ModelAndView res = new ModelAndView("advertisement/create");
        try {
            AdvertisementForm advertisementForm = new AdvertisementForm();
            res.addObject("advertisementForm", advertisementForm);
            res.addObject("newspapers", newspaperService.findAllNoDraft());
        } catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }

    @RequestMapping(value = "/create",method = RequestMethod.POST,params = "save")
    public ModelAndView save(@Valid AdvertisementForm advertisementForm, BindingResult result){
        ModelAndView res = new ModelAndView("advertisement/create");
        res.addObject("advertisementForm", advertisementForm);
        res.addObject("newspapers", newspaperService.findAllNoDraft());
        if (!result.hasErrors()){
            try{
                Advertisement advertisement = advertisementService.reconstruct(advertisementForm);
                advertisementService.save(advertisement);
                res = new ModelAndView("redirect:/newspaper/actor/view.do?newspaperId="+advertisementForm.getNewspaper().getId());
            } catch (Throwable oops){
                if ("creditcard.date.error".equals(oops.getMessage().toLowerCase())){
                    res.addObject("message","creditcard.date.error");
                }
            }
        }
        return res;
    }
}
