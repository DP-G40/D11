package controllers.agent;

import controllers.AbstractController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import services.NewspaperService;

@Controller
@RequestMapping("/newspaper/agent")
public class NewspaperAgentController extends AbstractController {

    @Autowired
    private NewspaperService newspaperService;

    @RequestMapping(value = "/listNoAdd",method = RequestMethod.GET)
    public ModelAndView listNoAdd(){
        ModelAndView res = new ModelAndView("newspaper/noAdd");
        res.addObject("newspapers",newspaperService.findAllWithNoAdd());
        return res;
    }

    @RequestMapping(value = "/listAdd",method = RequestMethod.GET)
    public ModelAndView listAdd(){
        ModelAndView res = new ModelAndView("newspaper/add");
        res.addObject("newspapers",newspaperService.findAllWithAdd());
        return res;
    }

}
