package controllers;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.VolumeService;
import domain.Volume;

@Controller
@RequestMapping("/volume")
public class VolumeController extends AbstractController {
    // Services --------------------------------------------
    @Autowired
    private VolumeService volumeService;

    // Constructor --------------------------------------
    public VolumeController() {
        super();
    }
// Listing Methods --------------------------------------

    @RequestMapping(value = "/view",method = RequestMethod.GET)
    public ModelAndView view(@RequestParam Integer volumeId){
        ModelAndView res = new ModelAndView("volume/view");
        try {
            Volume volume = volumeService.findOne(volumeId);
            res.addObject("volume",volume);
        } catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
        }
        return res;
    }

    @RequestMapping(value = "/list",method = RequestMethod.GET)
    public ModelAndView list(){
        ModelAndView res = new ModelAndView("volume/list");
        res.addObject("volumes",volumeService.findAll());
        return res;
    }


}
