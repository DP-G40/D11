package controllers.customer;

import java.util.Collection;
import java.util.List;

import controllers.AbstractController;
import domain.Article;
import domain.Customer;
import domain.Subscription;
import domain.User;
import forms.ArticleForm;
import forms.PictureForm;
import forms.SubscriptionForm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.ActorService;
import services.ArticleService;
import services.CustomerService;
import services.NewspaperService;
import services.PictureService;
import services.SubscriptionService;

import javax.validation.Valid;

@Controller
@RequestMapping("/subscription/customer")
public class SubscriptionUserController extends AbstractController{

    public SubscriptionUserController(){
        super();
    }

    @Autowired
    private SubscriptionService subscriptionService;

    @Autowired
    private NewspaperService newspaperService;

    @Autowired
    private ActorService actorService;
    
    @Autowired
    private CustomerService customerService;

    @RequestMapping(value = "/create",method = RequestMethod.GET)
    public ModelAndView create(@RequestParam Integer newspaperId){
        ModelAndView res = new ModelAndView("subscription/create");
        SubscriptionForm subscriptionForm = new SubscriptionForm();
        subscriptionForm.setNewspaperId(newspaperId);
        res.addObject("subscriptionForm",subscriptionForm);
        res.addObject("action","subscription/customer/create.do");
        return res;
    }

    @RequestMapping(value = "/create",method = RequestMethod.POST,params = "save")
    public ModelAndView saveCreate(@Valid SubscriptionForm subscriptionForm, BindingResult result){
        ModelAndView res = new ModelAndView("subscription/create");
        res.addObject("subscriptionForm",subscriptionForm);
        if(!result.hasErrors()){
            try{
                Subscription subscription = subscriptionService.reconstruct(subscriptionForm);
                subscriptionService.save(subscription);
                res = new ModelAndView("redirect:/newspaper/actor/list.do");
            } catch (Throwable oops){
                if ("creditcard.date.error".equals(oops.getMessage().toLowerCase())){
                    res.addObject("message","creditcard.date.error");
                }
            }
        }
        return res;
    }

    @RequestMapping(value = "/subscribeVolume",method = RequestMethod.GET)
    public ModelAndView subscribeVolume(@RequestParam Integer volumeId){
        ModelAndView res = new ModelAndView("subscription/create");
        SubscriptionForm subscriptionForm = new SubscriptionForm();
        subscriptionForm.setVolumeId(volumeId);
        res.addObject("subscriptionForm",subscriptionForm);
        res.addObject("action","subscription/customer/subscribeVolume.do");
        return res;
    }

    @RequestMapping(value = "/subscribeVolume",method = RequestMethod.POST,params = "save")
    public ModelAndView saveSubscribeVolume(@Valid SubscriptionForm subscriptionForm, BindingResult result){
        ModelAndView res = new ModelAndView("subscription/create");
        res.addObject("subscriptionForm",subscriptionForm);
        if(!result.hasErrors()){
            try{
                Subscription subscription = subscriptionService.reconstruct(subscriptionForm);
                subscription = subscriptionService.save(subscription);
                subscriptionService.subscribeNewspaperByVolume(subscription);
                res = new ModelAndView("redirect:/newspaper/actor/list.do");
            } catch (Throwable oops){
                if ("creditcard.date.error".equals(oops.getMessage().toLowerCase())){
                    res.addObject("message","creditcard.date.error");
                }
            }
        }
        return res;
    }

}
