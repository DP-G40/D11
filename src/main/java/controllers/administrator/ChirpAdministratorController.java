package controllers.administrator;

import controllers.AbstractController;
import controllers.ArticleController;
import domain.Article;
import domain.Chirp;
import domain.Newspaper;
import domain.User;
import forms.ArticleForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.ArticleService;
import services.ChirpService;
import services.NewspaperService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;

@Controller
@RequestMapping("/chirp/administrator")
public class ChirpAdministratorController extends AbstractController{

    @Autowired
    private ChirpService chirpService;


    @RequestMapping(value="/list")
    public ModelAndView listAll() {
        ModelAndView result;

        Collection<Chirp> chirps = chirpService.findAll();
        result = new ModelAndView("chirp/listall");
        result.addObject("chirps",chirps);
        result.addObject("requestURI", "/chirp/administrator/list.do");

        return result;
    }
    @RequestMapping(value = "/remove", method = RequestMethod.GET)
    public ModelAndView remove(@RequestParam int chirpId) {
        ModelAndView res = new ModelAndView("redirect:/chirp/administrator/list.do");

        try{
            chirpService.delete(chirpService.findOne(chirpId));
        }catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
            return res;
        }

        return res;
    }


}