package controllers.administrator;

import controllers.AbstractController;
import controllers.ArticleController;
import domain.Article;
import domain.Newspaper;
import domain.TabooWord;
import domain.User;
import forms.ArticleForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.ArticleService;
import services.ChirpService;
import services.NewspaperService;
import services.TabooWordService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;

@Controller
@RequestMapping("/tabooWord/administrator")
public class TabooWordsAdministratorController extends AbstractController{

    @Autowired
    private TabooWordService tabooWordService;
    
    @Autowired
    private ArticleService articleService;
    
    @Autowired
    private ChirpService chirpService;
    
    @Autowired
    private NewspaperService newspaperService;

    @RequestMapping(value="/list")
    public ModelAndView listAll() {
        ModelAndView result;

        Collection<TabooWord> tabooWords = tabooWordService.findAll();
        result = new ModelAndView("tabooWord/list");
        result.addObject("tabooWords", tabooWords);
        result.addObject("requestURI", "/tabooWord/administrator/list.do");
        
        //listar elementos que tienen taboowords
        
        result.addObject("tabooWordsArticles", articleService.containsTabooWord());
        result.addObject("tabooWordsNewspapers", newspaperService.containsTabooWord());
        result.addObject("tabooWordsChirps", chirpService.containsTabooWord());

        return result;
    }
    
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public ModelAndView remove(@RequestParam int tabooWordId) {
        ModelAndView res;

        try{
        	tabooWordService.delete(tabooWordService.findOne(tabooWordId));
        }catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
            return res;
        }

        return listAll();
    }
    
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public ModelAndView edit(@RequestParam int tabooWordId) {
        ModelAndView res;

        try{
        	res = new ModelAndView("tabooWord/create");
            res.addObject("tabooWord", tabooWordService.findOne(tabooWordId));
        }catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
            return res;
        }

        return res;
    }
    
    @RequestMapping(value = "/create")
    public ModelAndView create() {
        ModelAndView res;

        try{
        	res = new ModelAndView("tabooWord/create");
            res.addObject("tabooWord", tabooWordService.create());
        }catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
            return res;
        }

        return res;
    }
    
    @RequestMapping(value = "/save",method = RequestMethod.POST,params = "save")
    public ModelAndView save(@Valid TabooWord tabooWord, BindingResult result){
        ModelAndView res = new ModelAndView("tabooWord/create");
        if(!result.hasErrors()){
            try{
            	if(tabooWord.getId()==0 && tabooWordService.findByWord(tabooWord.getWord())!=null){
            		res.addObject("tabooWord", tabooWord);
            		res.addObject("message", "message.tabooWord.duplicate");
            	} else {
            		tabooWordService.save(tabooWord);
            		res = new ModelAndView("redirect:/tabooWord/administrator/list.do");	
            	}
            } catch (Throwable oops){

            }
        }else{
            res.addObject("tabooWord", tabooWord);
        }
        return res;
    }

}