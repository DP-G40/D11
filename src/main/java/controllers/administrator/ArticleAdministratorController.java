package controllers.administrator;

import controllers.AbstractController;
import controllers.ArticleController;
import domain.Article;
import domain.Newspaper;
import domain.User;
import forms.ArticleForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.ArticleService;
import services.NewspaperService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;

@Controller
@RequestMapping("/article/administrator")
public class ArticleAdministratorController extends AbstractController{

    @Autowired
    private ArticleService articleService;

    @RequestMapping(value="/list")
    public ModelAndView listAll() {
        ModelAndView result;

        Collection<Article> articles = articleService.findAll();
        result = new ModelAndView("article/list");
        result.addObject("articles", articles);
        result.addObject("requestURI", "/article/actor/list.do");

        return result;
    }

    @RequestMapping(value = "/remove", method = RequestMethod.GET)
    public ModelAndView remove(@RequestParam int articleId) {
        ModelAndView res;

        try{
            articleService.delete(articleService.findOne(articleId));
        }catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
            return res;
        }

        return listAll();
    }


}