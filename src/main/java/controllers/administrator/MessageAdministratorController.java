package controllers.administrator;

import controllers.AbstractController;
import domain.Actor;
import domain.Message;
import forms.BroadcastForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import services.ActorService;
import services.MessageService;

import javax.validation.Valid;

@Controller
@RequestMapping("/message/administrator")
public class MessageAdministratorController extends AbstractController {

    @Autowired
    private MessageService messageService;

    @Autowired
    private ActorService actorService;



    // Creating
    @RequestMapping(value = "/broadcast", method = RequestMethod.GET)
    public ModelAndView create() {
        ModelAndView result;
        Message mess;

        Actor actor;

        BroadcastForm broadcastForm = new BroadcastForm();
        actor = this.actorService.getPrincipal();

        result = new ModelAndView("broadcast/create");
        result.addObject("broadcastForm", broadcastForm);
        return result;
    }

    // Save
    @RequestMapping(value = "/broadcast", method = RequestMethod.POST, params = "save")
    public ModelAndView save(@Valid BroadcastForm broadcastForm, final BindingResult bindingResult) {
        ModelAndView result;

        Actor actor;
        actor = this.actorService.getPrincipal();

        if (bindingResult.hasErrors()) {
            result = new ModelAndView("broadcast/create");
            result.addObject("broadcastForm", broadcastForm);

        } else
            try {
                this.messageService.broadcast(broadcastForm);
                result = new ModelAndView("redirect:/folder/authenticate/list.do");
            } catch (final Throwable oops) {
                result = new ModelAndView("broadcast/create");
                result.addObject("broadcastForm", broadcastForm);
                result.addObject("message", "message.commit.error");

            }
        return result;
    }

}
