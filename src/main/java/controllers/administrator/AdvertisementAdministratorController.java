package controllers.administrator;

import controllers.AbstractController;
import controllers.ArticleController;
import domain.Advertisement;
import domain.Article;
import domain.Newspaper;
import domain.User;
import forms.ArticleForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import services.AdvertisementService;
import services.ArticleService;
import services.NewspaperService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;

@Controller
@RequestMapping("/advertisement/administrator")
public class AdvertisementAdministratorController extends AbstractController{

    @Autowired
    private AdvertisementService advertisementService;

    @RequestMapping(value="/listTaboo")
    public ModelAndView listAll() {
        ModelAndView result;

        Collection<Advertisement> advertisements = advertisementService.containsTabooWord();
        result = new ModelAndView("advertisement/list");
        result.addObject("advertisements", advertisements);
        result.addObject("requestURI", "/advertisement/administrator/list.do");

        return result;
    }

   @RequestMapping(value="/list")
    public ModelAndView list() {
        ModelAndView result;

        Collection<Advertisement> advertisements = advertisementService.findAll();
        result = new ModelAndView("advertisement/list");
        result.addObject("advertisements", advertisements);
        result.addObject("requestURI", "/advertisement/administrator/list.do");

        return result;
    }

    @RequestMapping(value = "/remove", method = RequestMethod.GET)
    public ModelAndView remove(@RequestParam int advertisementId) {
        ModelAndView res;

        try{
        	advertisementService.delete(advertisementService.findOne(advertisementId));
        }catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
            return res;
        }

        return list();
    }


}