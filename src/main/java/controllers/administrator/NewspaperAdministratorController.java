package controllers.administrator;

import controllers.AbstractController;
import domain.Article;
import domain.Newspaper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.ArticleService;
import services.NewspaperService;

import java.util.Collection;

@Controller
@RequestMapping("/newspaper/administrator")
public class NewspaperAdministratorController extends AbstractController{

    @Autowired
    private NewspaperService newspaperService;

    @RequestMapping(value="/list")
    public ModelAndView listAll() {
        ModelAndView result;

        Collection<Newspaper> newspapers = newspaperService.findAll();
        result = new ModelAndView("newspaper/list");
        result.addObject("newspapers", newspapers);
        result.addObject("requestURI", "/newspaper/actor/list.do");

        return result;
    }

    @RequestMapping(value = "/remove", method = RequestMethod.GET)
    public ModelAndView remove(@RequestParam int newspaperId) {
        ModelAndView res;

        try{
            newspaperService.delete(newspaperService.findOne(newspaperId));
        }catch (Throwable oops){
            res = new ModelAndView("redirect:/#");
            return res;
        }

        return listAll();
    }


}