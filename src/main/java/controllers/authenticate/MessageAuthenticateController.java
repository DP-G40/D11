
package controllers.authenticate;

import controllers.AbstractController;
import domain.Actor;
import domain.Folder;
import domain.Message;
import forms.MessageForm;
import forms.MessageMoveForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.ActorService;
import services.FolderService;
import services.MessageService;

import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("/message/authenticate")
public class MessageAuthenticateController extends AbstractController {

    // Constructor-----------------------------------------------------------------
    public MessageAuthenticateController() {
        super();
    }


    // Supporting
    // services---------------------------------------------------------
    @Autowired
    private ActorService actorService;

    @Autowired
    private MessageService messageService;

    @Autowired
    private FolderService folderService;


    // Listing
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list(@RequestParam final int folderId) {
        ModelAndView result;
        Collection<Message> messages;
        Folder folder;
        try {
            folder = this.folderService.findOne(folderId);

            messages = this.messageService.findMessagesByFolder(folderId);

            result = new ModelAndView("message/list");
            result.addObject("messages", messages);
            result.addObject("folder", folder);
            result.addObject("requestUri", "message/authenticate/list.do");
        } catch (final Throwable oops) {
            result = new ModelAndView("redirect:/#");
        }
        return result;
    }

    @RequestMapping(value = "/spam", method = RequestMethod.GET)
    public ModelAndView join(@RequestParam final int messageId) {
        ModelAndView result;
        final String rol = "authenticate";
        final Actor actor = this.actorService.getPrincipal();
        try {
            final Message message = this.messageService.findOne(messageId);
            this.messageService.checkPrincipalActor(message);
            this.messageService.spam(message);
            result = new ModelAndView("redirect:list.do?folderId=" + message.getFolder().getId());
            result.addObject("requestURI", "message/" + rol + "/spam.do?messageId=" + message.getId());
            result.addObject("rol", rol);
            result.addObject("message", message);
        } catch (final Throwable oops) {
            result = new ModelAndView("redirect:/#");
        }
        return result;
    }

    // Display
    @RequestMapping(value = "/display", method = RequestMethod.GET)
    public ModelAndView display(@RequestParam final int messageId) {
        ModelAndView result;
        Message mess;
        try {
            mess = this.messageService.findOneDisplay(messageId);
            result = new ModelAndView("message/display");
            result.addObject("mess", mess);
            result.addObject("folders", this.folderService.findFoldersToMoveByPrincipal());
            final MessageMoveForm messageMoveForm = new MessageMoveForm();
            messageMoveForm.setMessage(mess);
            result.addObject("messageMoveForm", messageMoveForm);
        } catch (final Throwable oops) {
            result = new ModelAndView("redirect:/#");
        }
        return result;
    }
    @RequestMapping(value = "/move", method = RequestMethod.POST, params = "save")
    public ModelAndView saveMove(@Valid final MessageMoveForm messageMoveForm, final BindingResult binding) {
        ModelAndView res = new ModelAndView("message/display");
        if (binding.hasErrors()) {
            res.addObject("messageMoveForm", messageMoveForm);
            res.addObject("mess", messageMoveForm.getMessage());
            res.addObject("folders", this.folderService.findFoldersToMoveByPrincipal());
        } else
            try {
                final Message message = this.messageService.moveMessage(messageMoveForm);
                this.messageService.save(message);
                res = new ModelAndView("redirect:/message/authenticate/list.do?folderId=" + messageMoveForm.getFolder().getId());
            } catch (final Throwable oops) {
                res.addObject("messageMoveForm", messageMoveForm);
                res.addObject("mess", messageMoveForm.getMessage());
                res.addObject("folders", this.folderService.findFoldersToMoveByPrincipal());
            }
        return res;
    }

    // Creating
    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView create() {
        ModelAndView result;
        Message mess;
        Collection<Actor> actors;
        Actor actor;

        mess = this.messageService.create();
        MessageForm messageForm = new MessageForm();
        actors = this.actorService.findAll();
        actor = this.actorService.getPrincipal();
        actors.remove(actor);

        result = new ModelAndView("message/create");
        result.addObject("messageForm", messageForm);
        result.addObject("actors", actors);
        return result;
    }

    // Save
    @RequestMapping(value = "/create", method = RequestMethod.POST, params = "save")
    public ModelAndView save(@Valid MessageForm messageForm, final BindingResult bindingResult) {
        ModelAndView result;
        Collection<Actor> actors;
        Actor actor;

        actors = this.actorService.findAll();
        actor = this.actorService.getPrincipal();
        actors.remove(actor);

        if (bindingResult.hasErrors()) {
            result = new ModelAndView("message/create");
            result.addObject("messageForm", messageForm);
            result.addObject("actors", actors);
        } else
            try {
                Message mess = messageService.reconstruct(messageForm);
                mess = this.messageService.saveToSend(mess,false);
                result = new ModelAndView("redirect:/folder/authenticate/list.do");
            } catch (final Throwable oops) {
                result = new ModelAndView("message/create");
                result.addObject("messageForm", messageForm);
                result.addObject("message", "message.commit.error");
                result.addObject("actors", actors);
            }
        return result;
    }

    // Delete
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public ModelAndView sendToTrashFolder(@RequestParam final int messageId) {
        ModelAndView result;
        Message message;
        int folderId;
        try {
            message = this.messageService.findOne(messageId);
            Assert.notNull(message);
            folderId = message.getFolder().getId();

            this.messageService.delete(messageId);

            result = new ModelAndView("redirect:/message/authenticate/list.do?folderId=" + folderId);
        } catch (final Throwable oops) {
            result = new ModelAndView("redirect:/#");
        }
        return result;
    }

    // Ancillary methods
    protected ModelAndView createEditModelAndView(final Message message) {
        ModelAndView result;

        result = this.createEditModelAndView(message, null);

        return result;
    }

    protected ModelAndView createEditModelAndView(final Message message, final String message2) {
        ModelAndView result;

        result = new ModelAndView("message/edit");
        result.addObject("message", message);
        result.addObject("message2", message2);

        return result;
    }

}
