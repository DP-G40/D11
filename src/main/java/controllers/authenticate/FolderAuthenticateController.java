
package controllers.authenticate;

import controllers.AbstractController;
import domain.Actor;
import domain.Folder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import services.ActorService;
import services.FolderService;

import javax.validation.Valid;
import java.util.Collection;

@Controller
@RequestMapping("/folder/authenticate")
public class FolderAuthenticateController extends AbstractController {

    //Constructor-----------------------------------------------------------------
    public FolderAuthenticateController() {
        super();
    }


    //Supporting services---------------------------------------------------------

    @Autowired
    private FolderService folderService;

    @Autowired
    private ActorService actorService;


    //Listing
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView list() {
        ModelAndView result;
        Collection<Folder> folders;

        folders = this.folderService.findFoldersByPrincipal();

        result = new ModelAndView("folder/list");
        result.addObject("folders", folders);
        result.addObject("requestUri", "folder/authenticate/list.do");

        return result;
    }

    //Creating
    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public ModelAndView create() {
        ModelAndView result;
        Folder folder;
        final Actor actor = this.actorService.getPrincipal();
        folder = this.folderService.create(actor);
        folder.setSystem(false);
        result = new ModelAndView("folder/create");
        result.addObject("folder", folder);

        return result;
    }

    //Editing ------------------------------------------------
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public ModelAndView edit(@RequestParam final int folderId) {
        ModelAndView result;
        Folder folder;
        try {
            folder = this.folderService.findOneEdit(folderId);
            Assert.notNull(folder);
            result = new ModelAndView("folder/edit");
            result.addObject("folder", folder);
        } catch (final Throwable oops) {
            result = new ModelAndView("redirect:/#");
        }
        return result;
    }

    //Save
    @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "save")
    public ModelAndView save(@Valid Folder folder, final BindingResult bindingResult) {
        ModelAndView result;

        if (bindingResult.hasErrors())
            result = this.createEditModelAndView(folder);
        else
            try {
                folder = this.folderService.save(folder);
                result = new ModelAndView("redirect:/folder/authenticate/list.do");
            } catch (final Throwable oops) {
                result = this.createEditModelAndView(folder, "folder.commit.error");
            }
        return result;
    }

    //Delete
    @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "delete")
    public ModelAndView delete(final Folder folder, final BindingResult binding) {
        ModelAndView result;

        if (binding.hasErrors())
            result = this.createEditModelAndView(folder);
        else
            try {
                this.folderService.delete(folder);
                result = new ModelAndView("redirect:/folder/authenticate/list.do");
            } catch (final Throwable oops) {
                result = this.createEditModelAndView(folder, "folder.commit.error");
            }
        return result;
    }

    //Ancillary methods
    protected ModelAndView createEditModelAndView(final Folder folder) {
        ModelAndView result;

        result = this.createEditModelAndView(folder, null);

        return result;
    }

    protected ModelAndView createEditModelAndView(final Folder folder, final String message) {
        ModelAndView result;

        result = new ModelAndView("folder/edit");
        result.addObject("folder", folder);
        result.addObject("message2", message);

        return result;
    }
}
