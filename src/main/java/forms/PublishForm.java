package forms;

import domain.Newspaper;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class PublishForm {

    private Newspaper newspaper;

    private Boolean privateFlag = false;

    @NotNull
    @Valid
    public Newspaper getNewspaper() {
        return newspaper;
    }

    public void setNewspaper(Newspaper newspaper) {
        this.newspaper = newspaper;
    }

    @NotNull
    public Boolean getPrivateFlag() {
        return privateFlag;
    }

    public void setPrivateFlag(Boolean privateFlag) {
        this.privateFlag = privateFlag;
    }
}
