package forms;

import domain.Newspaper;

import javax.validation.constraints.NotNull;

public class AddNewspaperForm {

    private Newspaper newspaper;

    private Integer volumeId;


    @NotNull
    public Integer getVolumeId() {
        return volumeId;
    }

    public void setVolumeId(Integer volumeForm) {
        this.volumeId = volumeForm;
    }

    @NotNull
    public Newspaper getNewspaper() {
        return newspaper;
    }

    public void setNewspaper(Newspaper newspaper) {
        this.newspaper = newspaper;
    }
}
