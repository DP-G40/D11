package forms;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

public class BroadcastForm {

    private String subject;
    private String body;
    private String priority;


    @NotNull
    @NotBlank
    public String getSubject() {

        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    @NotNull
    @NotBlank
    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @NotNull
    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

}
