package forms;

import domain.Folder;
import domain.Message;

import javax.validation.constraints.NotNull;

public class MessageMoveForm {

    Message message;
    Folder folder;
    @NotNull
    public Message getMessage() {
        return message;
    }
    public void setMessage(Message message) {
        this.message = message;
    }
    @NotNull
    public Folder getFolder() {
        return folder;
    }
    public void setFolder(Folder folder) {
        this.folder = folder;
    }


}
