package usecases;

import domain.Customer;
import domain.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import security.Authority;
import security.UserAccount;
import services.CustomerService;
import services.UserService;
import utilities.AbstractTest;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.Collection;

@ContextConfiguration(locations = {
        "classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class UnauthenticatedTestService2 extends AbstractTest{

    @Autowired
    private UserService userService;

    @Autowired
    private CustomerService customerService;


    /**
     *
     * @param name
     * @param surname
     * @param email
     * @param phone
     * @param username
     * @param password
     * @param expected
     *
     * Test que prueba el registro como usuario por parte de un usuario unautenticado
     * El caso positivo consiste en el registro con todos los datos correctos
     * El primer caso negativo simula el intento de registro sin aportar el nombre de usuario
     * El segundo caso negativo simula el intento de registro sin aportar la contraseņa
     */
    public void registerAsAgentTestCase(String name,String surname,String email,String phone,String username,String password,Class<?> expected){
        Class<?> caught = null;
        try{
            unauthenticate();
            Customer customer = customerService.create();
            customer.setName(name);
            customer.setSurname(surname);
            customer.setEmail(email);
            customer.setPhoneNumber(phone);
            UserAccount userAccount = new UserAccount();
            Collection<Authority> authorities = new ArrayList<Authority>();
            Authority authority = new Authority();
            authority.setAuthority(Authority.AGENT);
            authorities.add(authority);
            userAccount.setAuthorities(authorities);
            userAccount.setUsername(username);
            userAccount.setPassword(new Md5PasswordEncoder().encodePassword(password,null));
            customer.setUserAccount(userAccount);
            customerService.save(customer);
            this.flushTransaction();
        } catch (Throwable oops){
            caught = oops.getClass();
        }
        this.checkExceptions(expected,caught);
    }

    @Test()
    public void registerAsAgentTestDriver(){
        Object[][]cases={{"name","surname","email@mail.com","666666666","username1","password",null},
                {"name","surname","email@mail.com","666666666","","password",ConstraintViolationException.class},
                {"name","surname","email@mail.com","666666666","username3","password",ConstraintViolationException.class}};

        for(int i = 0; i < cases.length;i++){
            registerAsAgentTestCase((String) cases[i][0],(String) cases[i][1],(String) cases[i][2],(String) cases[i][3],(String) cases[i][4],(String) cases[i][5],(Class<?>) cases[i][6]);
        }
    }




    @Override
    public void setUp(){
        startTransaction();
    }

    @Override
    public void tearDown(){
        rollbackTransaction();
    }


}
