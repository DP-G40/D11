package usecases;

import domain.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import services.*;
import utilities.AbstractTest;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;
import java.util.Date;

@ContextConfiguration(locations = {
        "classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class UserTestService extends AbstractTest {

    @Autowired
    private NewspaperService newspaperService;

    @Autowired
    private ArticleService articleService;

    private static Newspaper newspaper = null;

    private static Article article = null;

    @Autowired
    private PictureService pictureService;

    @Autowired
    private ChirpService chirpService;

    @Autowired
    FollowUpService followUpService;

    @Override
    public void setUp(){
        startTransaction();
        authenticate("user1");
        Newspaper newspaperTest = newspaperService.create();
        newspaperTest.setTitle("title");
        newspaperTest.setDescription("description");
        newspaper = newspaperService.save(newspaperTest);
        flushTransaction();
        Article articleTest = articleService.create();
        articleTest.setTitle("titlle");
        articleTest.setSummary("summary");
        articleTest.setBody("body");
        articleTest.setNewspaper(newspaper);
        article = articleService.save(articleTest);
        flushTransaction();
        unauthenticate();

    }

    @Override
    public void tearDown(){
        rollbackTransaction();
    }


    /**
     *
     * @param title
     * @param description
     * @param username
     * @param url
     * @param expected
     *
     * Test que prueba el creado de un newspaper por parte de un usuario autenticado como User
     * El caso positivo consiste en el creado con todos los datos correctos
     * El primer caso negativo simula el intento de creado con el titulo nulo
     * El primer caso negativo simula el intento de creado con titulo vacio
     * El primer caso negativo simula el intento de creado con la descripcion nula
     * El primer caso negativo simula el intento de creado con la descripcion vacia
     * El primer caso negativo simula el intento de creado por parte de un usuario no autenticado
     * El primer caso negativo simula el intento de creado por parte de un adminsitrador
     * El primer caso negativo simula el intento de creado por parte de un usuario no existente
     * El primer caso negativo simula el intento de creado con la url de la picture invalida
     * El primer caso negativo simula el intento de creado con la url de la picture vacia
     */
    public void createNewspaperTest(String title,String description,String username,String url,Class<?> expected){
        Class<?> caught = null;
        try {
            authenticate(username);
            Newspaper newspaper = newspaperService.create();
            newspaper.setDescription(description);
            newspaper.setTitle(title);
            newspaper = newspaperService.save(newspaper);
            flushTransaction();
            Picture picture = new Picture();
            picture.setImage(url);
            picture.setNewspaper(newspaper);
            pictureService.save(picture);
            flushTransaction();
            unauthenticate();
        } catch (Throwable oops){
            caught = oops.getClass();
        }
        checkExceptions(expected,caught);
    }

    @Test
    public void createNewspaperTestDriver(){
        Object[][]cases={{"title","description","user1","https://cdn.shopify.com/s/files/1/0668/2853/products/WARRIORS_6_5_news.jpg?v=1497311341",null},
                {null,"description","user1","https://cdn.shopify.com/s/files/1/0668/2853/products/WARRIORS_6_5_news.jpg?v=1497311341",ConstraintViolationException.class},
                {"","description","user1","https://cdn.shopify.com/s/files/1/0668/2853/products/WARRIORS_6_5_news.jpg?v=1497311341",ConstraintViolationException.class},
                {"title","","user1","https://cdn.shopify.com/s/files/1/0668/2853/products/WARRIORS_6_5_news.jpg?v=1497311341",ConstraintViolationException.class},
                {"title",null,"user1","https://cdn.shopify.com/s/files/1/0668/2853/products/WARRIORS_6_5_news.jpg?v=1497311341",ConstraintViolationException.class},
                {"title","description","","https://cdn.shopify.com/s/files/1/0668/2853/products/WARRIORS_6_5_news.jpg?v=1497311341",IllegalArgumentException.class},
                {"title","description","admin","https://cdn.shopify.com/s/files/1/0668/2853/products/WARRIORS_6_5_news.jpg?v=1497311341",ClassCastException.class},
                {"title","description","usuarioInventado","https://cdn.shopify.com/s/files/1/0668/2853/products/WARRIORS_6_5_news.jpg?v=1497311341",IllegalArgumentException.class},
                {"title","description","user1","picture",ConstraintViolationException.class},
                {"title","description","user1","",ConstraintViolationException.class}};

        for(int i = 0; i < cases.length;i++){
            createNewspaperTest((String) cases[i][0],(String) cases[i][1],(String) cases[i][2],(String)cases[i][3],(Class<?>) cases[i][4]);
        }
    }

    /**
     *
     * @param newspaper
     * @param username
     * @param article
     * @param privateFlag
     * @param expected
     *
     *
     * Test que prueba la publicacion de un newspaper por parte de un usuario autenticado como User
     * El caso positivo consiste en la publicacion con todos los datos correctos
     * El primer caso negativo simula el intento de publicacion con el newspaper nulo
     */
    public void publishNewspaperTest(Newspaper newspaper,String username,Article article,Boolean privateFlag,Class<?> expected){
        Class<?> caught = null;
        try {
            authenticate(username);
            newspaper.getArticles().add(article);
            newspaper.setDraft(false);
            newspaperService.publish(newspaper,privateFlag);
            flushTransaction();
            unauthenticate();
        } catch (Throwable oops){
            caught = oops.getClass();
        }
        checkExceptions(expected,caught);
    }

    @Test
    public void publishNewspaperTestDriver(){
        Object[][]cases={{newspaper,"user1",article,true,null},
                {null,"user1",article,true,NullPointerException.class}};

        for(int i = 0; i < cases.length;i++){
            publishNewspaperTest((Newspaper) cases[i][0],(String) cases[i][1],(Article) cases[i][2],(Boolean) cases[i][3],(Class<?>) cases[i][4]);
        }
    }

    /**
     *
     * @param title
     * @param summary
     * @param body
     * @param username
     * @param newspaper
     * @param expected
     *
     * Test que prueba el creado de un article por parte de un usuario autenticado como User
     * El caso positivo consiste en el creado con todos los datos correctos
     * El primer caso negativo simula el intento de creado con el titulo nulo
     */
    public void createArticleTest(String title, String summary,String body,String username,Newspaper newspaper,Class<?> expected){
        Class<?> caught = null;
        try {
            authenticate(username);
            Article article = articleService.create();
            article.setTitle(title);
            article.setSummary(summary);
            article.setBody(body);
            article.setNewspaper(newspaper);
            articleService.save(article);
            flushTransaction();
            unauthenticate();
        } catch (Throwable oops){
            caught = oops.getClass();
        }
        checkExceptions(expected,caught);
    }


    @Test
    public void createArticleTestDriver(){
        Object[][]cases={{"title","summary","body","user1",newspaper,null},
                {null,"summary","body","user1",newspaper,ConstraintViolationException.class}};

        for(int i = 0; i < cases.length;i++){
            createArticleTest((String) cases[i][0],(String) cases[i][1],(String) cases[i][2],(String) cases[i][3],(Newspaper) cases[i][4],(Class<?>) cases[i][5]);
        }
    }

    /**
     *
     * @param username
     * @param description
     * @param title
     * @param expected
     *
     * Test que prueba el creado de un chirp por parte de un usuario autenticado como User
     * El caso positivo consiste en el creado con todos los datos correctos
     * El primer caso negativo simula el intento de creado con el titulo nulo
     */
    public void postChirpTest(String username,String description,String title,Class<?> expected){
        Class<?> caught = null;
        try {
            authenticate(username);
            Chirp chirp = chirpService.create();
            chirp.setDescription(description);
            chirp.setTitle(title);
            chirp.setPublicationDate(new Date());
            chirpService.save(chirp);
            flushTransaction();
            unauthenticate();
        } catch (Throwable oops){
            caught = oops.getClass();
        }
        checkExceptions(expected,caught);
    }

    @Test
    public void postChirpTestDriver(){
        Object[][]cases={{"user1","title","description",null},
                {"user1",null,"description",ConstraintViolationException.class}};

        for(int i = 0; i < cases.length;i++){
            postChirpTest((String) cases[i][0],(String) cases[i][1],(String) cases[i][2],(Class<?>) cases[i][3]);
        }
    }

    /**
     *
     * @param username
     * @param article
     * @param title
     * @param summary
     * @param text
     * @param expected
     *
     *
     * Test que prueba el creado de un followUp por parte de un usuario autenticado como User
     * El caso positivo consiste en el creado con todos los datos correctos
     * El primer caso negativo simula el intento de creado con el article nulo
     */
    public void createFollowUpTest(String username,Article article,String title,String summary,String text,Class<?> expected){
        Class<?> caught = null;
        try {
            authenticate(username);
            FollowUp followUp = followUpService.create();
            followUp.setArticle(article);
            followUp.setSummary(summary);
            followUp.setText(text);
            followUp.setTitle(title);
            followUp.setPublicationDate(new Date());
            followUpService.save(followUp);
            flushTransaction();
            unauthenticate();
        } catch (Throwable oops){
            caught = oops.getClass();
        }
        checkExceptions(expected,caught);
    }

    @Test
    public void createFollowUpTestDriver(){
        Object[][]cases={{"user1",article,"title","summary","text",null},
                {"user1",null,"title","summary","text",ConstraintViolationException.class}};

        for(int i = 0; i < cases.length;i++){
            createFollowUpTest((String) cases[i][0],(Article) cases[i][1],(String) cases[i][2],(String) cases[i][3],(String) cases[i][4],(Class<?>) cases[i][5]);
        }
    }

}
