package usecases;

import domain.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import services.*;
import utilities.AbstractTest;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;
import java.util.Date;

@ContextConfiguration(locations = {
        "classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class UserTestService2 extends AbstractTest {

    @Autowired
    private NewspaperService newspaperService;

    @Autowired
    private ArticleService articleService;

    private static Newspaper newspaper = null;

    private static Article article = null;

    @Autowired
    private PictureService pictureService;

    @Autowired
    private ChirpService chirpService;

    @Autowired
    private FollowUpService followUpService;

    @Autowired
    private VolumeService volumeService;

    @Override
    public void setUp(){
        startTransaction();
        authenticate("user1");
        Newspaper newspaperTest = newspaperService.create();
        newspaperTest.setTitle("title");
        newspaperTest.setDescription("description");
        newspaper = newspaperService.save(newspaperTest);
        flushTransaction();
        Article articleTest = articleService.create();
        articleTest.setTitle("titlle");
        articleTest.setSummary("summary");
        articleTest.setBody("body");
        articleTest.setNewspaper(newspaper);
        article = articleService.save(articleTest);
        flushTransaction();
        unauthenticate();

    }

    @Override
    public void tearDown(){
        rollbackTransaction();
    }


    /**
     *
     * @param title
     * @param description
     * @param username
     * @param newspaper
     * @param year
     * @param expected
     */
    public void createVolumeTest(String title,String description,String username,Newspaper newspaper,String year,Class<?> expected){
        Class<?> caught = null;
        try {
            authenticate(username);
            Volume vol = volumeService.create();
            vol.setDescription(description);
            vol.setTitle(title);
            vol.setYear(year);
            vol = volumeService.save(vol);
            flushTransaction();
            vol.getNewspapers().add(newspaper);
            volumeService.save(vol);
            flushTransaction();
            unauthenticate();
        } catch (Throwable oops){
            caught = oops.getClass();
        }
        checkExceptions(expected,caught);
    }

    @Test
    public void createVolumeTestDriver(){
        Object[][]cases={{"title","description","user1",newspaper,"2018",null},
                {null,"description","user1",newspaper,"2018",ConstraintViolationException.class},
                {"","description","user1",newspaper,"2018",ConstraintViolationException.class},
                {"title","","user1",newspaper,"2018",ConstraintViolationException.class},
                {"title",null,"user1",newspaper,"2018",ConstraintViolationException.class},
                {"title","description","",newspaper,"2018",IllegalArgumentException.class},
                {"title","description","admin",newspaper,"2018",ClassCastException.class},
                {"title","description","usuarioInventado",newspaper,"2018",IllegalArgumentException.class},
                {"title","description","user1",newspaper,null,ConstraintViolationException.class},
                {"title","description","user1",null,"2018",ConstraintViolationException.class}};

        for(int i = 0; i < cases.length;i++){
            createVolumeTest((String) cases[i][0],(String) cases[i][1],(String) cases[i][2],(Newspaper) cases[i][3],(String) cases[i][4],(Class<?>) cases[i][5]);
        }
    }


}
