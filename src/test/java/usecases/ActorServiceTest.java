package usecases;

import domain.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import services.ActorService;
import services.FolderService;
import services.MessageService;
import utilities.AbstractTest;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

@ContextConfiguration(locations = {
        "classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class ActorServiceTest extends AbstractTest {

    @Autowired
    private MessageService messageService;

    @Autowired
    private ActorService actorService;

    @Autowired
    private FolderService folderService;

    private static Folder folder;

    public void sendMessageTest(String sender,String receiver,String subject,String body ,String priority,Class<?> expected){
        Class<?> caught = null;
        try{
            authenticate(sender);
            Message mes = messageService.create();
            mes.setBody(body);
            mes.setPriority(priority);
            mes.setSubject(subject);
            mes.setRecipient(actorService.getByUserName(receiver));
            messageService.saveToSend(mes,false);
            flushTransaction();
        } catch (Throwable oops){
            caught = oops.getClass();
        }
        this.checkExceptions(expected,caught);
    }

    @Test
    public void sendMessageDriver(){
        Object[][]cases={{"admin","admin","subject","body","LOW",null},
                {"admin","admin","subject","body","",ConstraintViolationException.class}};

        for(int i = 0; i < cases.length;i++){
            sendMessageTest((String) cases[i][0],(String) cases[i][1],(String) cases[i][2],(String) cases[i][3],(String) cases[i][4],(Class<?>) cases[i][5]);
        }
    }

    public void createFolderTest(String username,String name,Class<?> expected){
        Class<?> caught = null;
        try{
            authenticate(username);
            Folder folder = folderService.create(actorService.findByPrincipal());
            folder.setName(name);
            folderService.save(folder);
            flushTransaction();
        } catch (Throwable oops){
            caught = oops.getClass();
        }
        this.checkExceptions(expected,caught);
    }

    @Test
    public void createFolderDriver(){
        Object[][]cases={{"admin","folder",null},
                {null,"folder",IllegalArgumentException.class}};

        for(int i = 0; i < cases.length;i++){
            createFolderTest((String) cases[i][0],(String) cases[i][1],(Class<?>) cases[i][2]);
        }
    }

    public void deleteFolderTest(String username,Folder folder,Class<?> expected){
        Class<?> caught = null;
        try{
            authenticate(username);
            folderService.delete(folder);
            flushTransaction();
        } catch (Throwable oops){
            caught = oops.getClass();
        }
        this.checkExceptions(expected,caught);
    }

    @Test
    public void deleteFolderDriver(){
        Object[][]cases={{"admin",folder,null},
                {null,null,IllegalArgumentException.class}};

        for(int i = 0; i < cases.length;i++){
            deleteFolderTest((String) cases[i][0],(Folder) cases[i][1],(Class<?>) cases[i][2]);
        }
    }

    @Override
    public void setUp(){
        startTransaction();
        authenticate("admin");
        Folder fol = folderService.create(actorService.findByPrincipal());
        fol.setName("folder de prueba");
        fol.setSystem(false);
        folder = folderService.save(fol);
        flushTransaction();
    }

    @Override
    public void tearDown(){
        rollbackTransaction();
    }

}
