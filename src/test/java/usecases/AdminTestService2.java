package usecases;

import domain.*;
import forms.BroadcastForm;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import services.*;
import utilities.AbstractTest;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;
import java.util.Date;

@ContextConfiguration(locations = {
        "classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class AdminTestService2 extends AbstractTest {

    @Autowired
    private NewspaperService newspaperService;

    @Autowired
    private ArticleService articleService;

    @Autowired
    private ChirpService chirpService;

    @Autowired
    private TabooWordService tabooWordService;

    @Autowired
    private AdvertisementService advertisementService;

    @Autowired
    private MessageService messageService;

    @Autowired
    private ActorService actorService;

    private static Newspaper newspaper = null;

    private static Article article = null;

    private static Chirp chirp = null;

    private static TabooWord tabooWord = null;

    private static Advertisement advertisement = null;

    private static CreditCard creditCard = null;

    /**
     *
     * @param advertisement
     * @param expected
     *
     * Test que prueba el borrado de un newspaper por parte de un administrador
     * El caso positivo consiste en el borrado con todos los datos correctos
     * El primer caso negativo simula el intento de borrado de un newspaper nulo
     */
    public void deleteAdvertisementTest(Advertisement advertisement,Class<?> expected){
        Class<?> caught = null;
        try {
            authenticate("admin");
            advertisementService.delete(advertisement);
            flushTransaction();
        } catch (Throwable oops){
            caught = oops.getClass();
        }
        checkExceptions(expected,caught);
    }

    @Test
    public void deleteAdvertisementTestDriver(){
        Object[][]cases={{advertisement,null},
                {null,IllegalArgumentException.class}};

        for(int i = 0; i < cases.length;i++){
            deleteAdvertisementTest((Advertisement) cases[i][0],(Class<?>) cases[i][1]);
        }
    }

    public void broadcastTest(String sender,String subject,String body ,String priority,Class<?> expected){
        Class<?> caught = null;
        try{
            authenticate(sender);
            BroadcastForm broadcastForm = new BroadcastForm();
            broadcastForm.setBody(body);
            broadcastForm.setPriority(priority);
            broadcastForm.setSubject(subject);
            messageService.broadcast(broadcastForm);
            flushTransaction();
        } catch (Throwable oops){
            caught = oops.getClass();
        }
        this.checkExceptions(expected,caught);
    }

    @Test
    public void broadcastDriver(){
        Object[][]cases={{"admin","subject","body","LOW",null},
                {"admin","subject","body","",ConstraintViolationException.class}};

        for(int i = 0; i < cases.length;i++){
            broadcastTest((String) cases[i][0],(String) cases[i][1],(String) cases[i][2],(String) cases[i][3],(Class<?>) cases[i][4]);
        }
    }






    @Override
    public void setUp(){
        startTransaction();
        authenticate("user1");
        Newspaper newspaperTest = newspaperService.create();
        newspaperTest.setTitle("title");
        newspaperTest.setDescription("description");
        newspaper = newspaperService.save(newspaperTest);
        flushTransaction();
        Article articleTest = articleService.create();
        articleTest.setTitle("title");
        articleTest.setSummary("summary");
        articleTest.setBody("body");
        articleTest.setNewspaper(newspaper);
        article = articleService.save(articleTest);
        flushTransaction();
        Chirp chirpTest = chirpService.create();
        chirpTest.setTitle("title");
        chirpTest.setDescription("description");
        chirpTest.setPublicationDate(new Date());
        chirp = chirpService.save(chirpTest);
        flushTransaction();
        TabooWord tabooWordTest = new TabooWord();
        tabooWordTest.setWord("word");
        tabooWord = tabooWordService.save(tabooWordTest);
        flushTransaction();
        creditCard = new CreditCard();
        creditCard.setHolderName("holder");
        creditCard.setBrand("brand");
        creditCard.setNumber("4027240459748441");
        creditCard.setCvv(100);
        creditCard.setExpiryMonth(12);
        creditCard.setExpiryYear(2020);
        authenticate("agent1");
        Advertisement add = advertisementService.create();
        add.setNewspaper(newspaper);
        add.setUrlPage("http://www.google.es");
        add.setUrlBanner("http://www.google.es");
        add.setTitle("title");
        add.setCreditcard(creditCard);
        advertisement = advertisementService.save(add);
        flushTransaction();
        unauthenticate();

    }

    @Override
    public void tearDown(){
        rollbackTransaction();
    }


}
