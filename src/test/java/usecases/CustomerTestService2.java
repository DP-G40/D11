package usecases;

import domain.CreditCard;
import domain.Newspaper;
import domain.Subscription;
import domain.Volume;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import services.NewspaperService;
import services.SubscriptionService;
import services.VolumeService;
import utilities.AbstractTest;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

@ContextConfiguration(locations = {
        "classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class CustomerTestService2 extends AbstractTest{

    @Autowired
    private SubscriptionService subscriptionService;

    @Autowired
    private NewspaperService newspaperService;

    @Autowired
    private VolumeService volumeService;

    private static Newspaper newspaper = null;

    public static CreditCard creditCard;

    public static Volume volume;


    /**
     *
     * @param creditCard
     * @param expected
     *
     * *Test que prueba el creado de una subscription por parte de un customer
     * El caso positivo consiste en el creado con todos los datos correctos
     * El primer caso negativo simula el intento de creado con una creditcard nula
     */
    public void subscribeToVolumeTest(CreditCard creditCard, Volume volume, Class<?> expected){
        Class<?> caught = null;
        try{
            authenticate("customer1");
            Subscription subscription = subscriptionService.create();
            subscription.setVolume(volume);
            subscription.setCreditcard(creditCard);
            subscriptionService.save(subscription);
            this.flushTransaction();
        } catch (Throwable oops){
            caught = oops.getClass();
        }
        this.checkExceptions(expected,caught);
    }

    @Test
    public void subscribeToVolumeDriver(){
        Object[][]cases={{creditCard,volume,null},
                {null,volume,ConstraintViolationException.class}};

        for(int i = 0; i < cases.length;i++){
            subscribeToVolumeTest((CreditCard) cases[i][0],(Volume) cases[i][1],(Class<?>) cases[i][2]);
        }
    }


    @Override
    public void setUp(){
        startTransaction();
        authenticate("user1");
        Newspaper newspaperTest = newspaperService.create();
        newspaperTest.setTitle("title");
        newspaperTest.setDescription("description");
        newspaper = newspaperService.save(newspaperTest);
        flushTransaction();
        creditCard = new CreditCard();
        creditCard.setHolderName("holder");
        creditCard.setBrand("brand");
        creditCard.setNumber("4027240459748441");
        creditCard.setCvv(100);
        creditCard.setExpiryMonth(12);
        creditCard.setExpiryYear(2020);
        Volume vol = volumeService.create();
        vol.setYear("2018");
        vol.setTitle("title");
        vol.setDescription("description");
        volume = volumeService.save(vol);
        flushTransaction();

    }

    @Override
    public void tearDown(){
        rollbackTransaction();
    }
}
