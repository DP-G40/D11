package usecases;

import domain.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import security.Authority;
import security.UserAccount;
import services.CustomerService;
import services.NewspaperService;
import services.SubscriptionService;
import utilities.AbstractTest;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.Collection;

@ContextConfiguration(locations = {
        "classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class CustomerTestService extends AbstractTest{

    @Autowired
    private SubscriptionService subscriptionService;

    @Autowired
    private NewspaperService newspaperService;

    private static Newspaper newspaper = null;

    public static CreditCard creditCard;


    /**
     *
     * @param creditCard
     * @param expected
     *
     * *Test que prueba el creado de una subscription por parte de un customer
     * El caso positivo consiste en el creado con todos los datos correctos
     * El primer caso negativo simula el intento de creado con una creditcard nula
     */
    public void subscribeToPrivateNewspaperTest(CreditCard creditCard, Newspaper newspaper,Class<?> expected){
        Class<?> caught = null;
        try{
            authenticate("customer1");
            Subscription subscription = subscriptionService.create();
            subscription.setNewspaper(newspaper);
            subscription.setCreditcard(creditCard);
            subscriptionService.save(subscription);
            this.flushTransaction();
        } catch (Throwable oops){
            caught = oops.getClass();
        }
        this.checkExceptions(expected,caught);
    }

    @Test
    public void subscribeToPrivateNewspaperDriver(){
        Object[][]cases={{creditCard,newspaper,null},
                {null,newspaper,ConstraintViolationException.class}};

        for(int i = 0; i < cases.length;i++){
            subscribeToPrivateNewspaperTest((CreditCard) cases[i][0],(Newspaper) cases[i][1],(Class<?>) cases[i][2]);
        }
    }


    @Override
    public void setUp(){
        startTransaction();
        authenticate("user1");
        Newspaper newspaperTest = newspaperService.create();
        newspaperTest.setTitle("title");
        newspaperTest.setDescription("description");
        newspaper = newspaperService.save(newspaperTest);
        flushTransaction();
        creditCard = new CreditCard();
        creditCard.setHolderName("holder");
        creditCard.setBrand("brand");
        creditCard.setNumber("4027240459748441");
        creditCard.setCvv(100);
        creditCard.setExpiryMonth(12);
        creditCard.setExpiryYear(2020);

    }

    @Override
    public void tearDown(){
        rollbackTransaction();
    }
}
