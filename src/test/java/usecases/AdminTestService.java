package usecases;

import domain.Article;
import domain.Chirp;
import domain.Newspaper;
import domain.TabooWord;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import services.ArticleService;
import services.ChirpService;
import services.NewspaperService;
import services.TabooWordService;
import utilities.AbstractTest;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;
import java.util.Date;

@ContextConfiguration(locations = {
        "classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class AdminTestService extends AbstractTest {

    @Autowired
    private NewspaperService newspaperService;

    @Autowired
    private ArticleService articleService;

    @Autowired
    private ChirpService chirpService;

    @Autowired
    private TabooWordService tabooWordService;

    private static Newspaper newspaper = null;

    private static Article article = null;

    private static Chirp chirp = null;

    private static TabooWord tabooWord = null;

    /**
     *
     * @param newspaper
     * @param expected
     *
     * Test que prueba el borrado de un newspaper por parte de un administrador
     * El caso positivo consiste en el borrado con todos los datos correctos
     * El primer caso negativo simula el intento de borrado de un newspaper nulo
     */
    public void deleteNewspaperTest(Newspaper newspaper,Class<?> expected){
        Class<?> caught = null;
        try {
            authenticate("admin");
            newspaperService.delete(newspaper);
        } catch (Throwable oops){
            caught = oops.getClass();
        }
        checkExceptions(expected,caught);
    }

    @Test
    public void deleteNewspaperTestDriver(){
        Object[][]cases={{newspaper,null},
                {null,IllegalArgumentException.class}};

        for(int i = 0; i < cases.length;i++){
            deleteNewspaperTest((Newspaper)cases[i][0],(Class<?>) cases[i][1]);
        }
    }

    /**
     *
     * @param article
     * @param expected
     *
     * *Test que prueba el borrado de un artcile por parte de un administrador
     * El caso positivo consiste en el borrado con todos los datos correctos
     * El primer caso negativo simula el intento de borrado de un article nulo
     */
    public void deleteArticleTest(Article article, Class<?> expected){
        Class<?> caught = null;
        try {
            authenticate("admin");
            articleService.delete(article);
        } catch (Throwable oops){
            caught = oops.getClass();
        }
        checkExceptions(expected,caught);
    }

    @Test
    public void deleteArticleTestDriver(){
        Object[][]cases={{article,null},
                {null,IllegalArgumentException.class}};

        for(int i = 0; i < cases.length;i++){
            deleteArticleTest((Article)cases[i][0],(Class<?>) cases[i][1]);
        }
    }

    /**
     *
     * @param chirp
     * @param expected
     *
     * Test que prueba el borrado de un artcile por parte de un administrador
     * El caso positivo consiste en el borrado con todos los datos correctos
     * El primer caso negativo simula el intento de borrado de un article nulo
     */
    public void deleteChirpTest(Chirp chirp, Class<?> expected){
        Class<?> caught = null;
        try {
            authenticate("admin");
            chirpService.delete(chirp);
            unauthenticate();
        } catch (Throwable oops){
            caught = oops.getClass();
        }
        checkExceptions(expected,caught);

    }



    @Test
    public void deleteChirpTestDriver(){
        Object[][]cases={{chirp,null},
                {null,IllegalArgumentException.class}};

        for(int i = 0; i < cases.length;i++){
            deleteChirpTest((Chirp)cases[i][0],(Class<?>) cases[i][1]);
        }
    }


    /**
     * @param word
     * @param expected
     *
     * Test que prueba el creado de una tabooWord por parte de un administrador
     * El caso positivo consiste en el creado con todos los datos correctos
     * El primer caso negativo simula el intento de creado de la word nula
     */
    public void createTabooWordTest(String word,Class<?> expected){
        Class<?> caught = null;
        try {
            authenticate("admin");
            TabooWord tabooWord = tabooWordService.create();
            tabooWord.setWord(word);
            tabooWordService.save(tabooWord);
            flushTransaction();
            unauthenticate();
        } catch (Throwable oops){
            caught = oops.getClass();
        }
        checkExceptions(expected,caught);
    }


    @Test
    public void createTabooWord(){
        Object[][]cases={{"word",null},
                {null,ConstraintViolationException.class}};

        for(int i = 0; i < cases.length;i++){
            createTabooWordTest((String)cases[i][0],(Class<?>) cases[i][1]);
        }
    }

    /**
     *
     * @param tabooWord
     * @param expected
     *
     * Test que prueba el borrado de una tabooWord por parte de un administrador
     * El caso positivo consiste en el borrado con todos los datos correctos
     * El primer caso negativo simula el intento de borrado de una tabooWord nulo
     */
    public void deleteTabooWordTest(TabooWord tabooWord,Class<?> expected){
        Class<?> caught = null;
        try {
            authenticate("admin");
            tabooWordService.delete(tabooWord);
            flushTransaction();
            unauthenticate();
        } catch (Throwable oops){
            caught = oops.getClass();
        }
        checkExceptions(expected,caught);
    }

    @Test
    public void deleteTabooWordTestDriver(){
        Object[][]cases={{tabooWord,null},
                {null,IllegalArgumentException.class}};

        for(int i = 0; i < cases.length;i++){
            deleteTabooWordTest((TabooWord) cases[i][0],(Class<?>) cases[i][1]);
        }
    }




    @Override
    public void setUp(){
        startTransaction();
        authenticate("user1");
        Newspaper newspaperTest = newspaperService.create();
        newspaperTest.setTitle("title");
        newspaperTest.setDescription("description");
        newspaper = newspaperService.save(newspaperTest);
        flushTransaction();
        Article articleTest = articleService.create();
        articleTest.setTitle("titlle");
        articleTest.setSummary("summary");
        articleTest.setBody("body");
        articleTest.setNewspaper(newspaper);
        article = articleService.save(articleTest);
        flushTransaction();
        Chirp chirpTest = chirpService.create();
        chirpTest.setTitle("title");
        chirpTest.setDescription("description");
        chirpTest.setPublicationDate(new Date());
        chirp = chirpService.save(chirpTest);
        flushTransaction();
        TabooWord tabooWordTest = new TabooWord();
        tabooWordTest.setWord("word");
        tabooWord = tabooWordService.save(tabooWordTest);
        flushTransaction();
        unauthenticate();

    }

    @Override
    public void tearDown(){
        rollbackTransaction();
    }


}
