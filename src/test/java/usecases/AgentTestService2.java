package usecases;

import domain.Advertisement;
import domain.CreditCard;
import domain.Newspaper;
import domain.Subscription;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import services.AdvertisementService;
import services.NewspaperService;
import utilities.AbstractTest;

import javax.transaction.Transactional;
import javax.validation.ConstraintViolationException;

@ContextConfiguration(locations = {
        "classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class AgentTestService2 extends AbstractTest {

    @Autowired
    private NewspaperService newspaperService;

    @Autowired
    private AdvertisementService advertisementService;

    private static Newspaper newspaper = null;

    public static CreditCard creditCard;

    @Override
    public void setUp(){
        startTransaction();
        authenticate("user1");
        Newspaper newspaperTest = newspaperService.create();
        newspaperTest.setTitle("title");
        newspaperTest.setDescription("description");
        newspaper = newspaperService.save(newspaperTest);
        unauthenticate();
        flushTransaction();
        creditCard = new CreditCard();
        creditCard.setHolderName("holder");
        creditCard.setBrand("brand");
        creditCard.setNumber("4027240459748441");
        creditCard.setCvv(100);
        creditCard.setExpiryMonth(12);
        creditCard.setExpiryYear(2020);
    }

    @Override
    public void tearDown(){
        rollbackTransaction();
    }

    public void createAdvertisementTest(String agent,String title,String urlPage,String urlBanner,Class<?> expected){
        Class<?> caught = null;
        try{
            authenticate(agent);
            Advertisement advertisement = advertisementService.create();
            advertisement.setNewspaper(newspaper);
            advertisement.setUrlPage(urlPage);
            advertisement.setUrlBanner(urlBanner);
            advertisement.setTitle(title);
            advertisement.setCreditcard(creditCard);
            this.flushTransaction();
        } catch (Throwable oops){
            caught = oops.getClass();
        }
        this.checkExceptions(expected,caught);
    }

    @Test
    public void createAdvertisementDriver(){
        Object[][]cases={{"agent1","title","http://www.google.es","http://www.google.es",null},
                {null,"title","http://www.google.es","http://www.google.es",IllegalArgumentException.class}};

        for(int i = 0; i < cases.length;i++){
            createAdvertisementTest((String) cases[i][0],(String) cases[i][1],(String) cases[i][2],(String) cases[i][3],(Class<?>) cases[i][4]);
        }
    }
}
